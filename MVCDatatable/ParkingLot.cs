
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace MVCDatatable
{

using System;
    using System.Collections.Generic;
    
public partial class ParkingLot
{

    public int Id { get; set; }

    public Nullable<int> MaxCode { get; set; }

    public string LotId { get; set; }

    public string LotName { get; set; }

    public string Address { get; set; }

    public string City { get; set; }

    public string State { get; set; }

    public string Country { get; set; }

    public string ZipCode { get; set; }

    public Nullable<int> Capacity { get; set; }

    public string DistanceFromVanue { get; set; }

    public string StartTime { get; set; }

    public string EndTime { get; set; }

    public Nullable<decimal> EventPrice { get; set; }

    public Nullable<decimal> HourPrice { get; set; }

    public string Lat { get; set; }

    public string Long { get; set; }

    public string MapLink { get; set; }

    public Nullable<bool> Status { get; set; }

    public Nullable<System.DateTime> CreatedDate { get; set; }

    public string CreatedBy { get; set; }

    public Nullable<System.DateTime> ModifiedDate { get; set; }

    public string ModifiedBy { get; set; }

}

}
