﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCDatatable.Models
{
    public class Login
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }

    public class ChangePassword
    {
        public string OPassword { get; set; }
        public string NPassword { get; set; }
        public string CPassword { get; set; }
    }
}