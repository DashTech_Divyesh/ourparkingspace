﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCDatatable.Models
{
    public class Parkinglot
    {
        public int ParkingLotId { get; set; }
        public string ParkingLotName { get; set; }
    }
}