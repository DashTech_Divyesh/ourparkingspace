﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCDatatable.Models
{
    public class EventInfo
    {
        public string EventName { get; set; }
        public DateTime EventDate { get; set; }
        public string dateofEvent { get; set; }
    }
}