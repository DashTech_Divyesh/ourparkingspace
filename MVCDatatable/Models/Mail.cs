﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Net;
using System.Net.Mime;
using System.IO;

namespace MVCDatatable
{
    public class Mail
    {
        //public string From { get; set; }
        //public string To { get; set; }
        //public string Body { get; set; }
        //public string subject { get; set; }

        public static string fromaddId = ""; 
        public static string fromaddPass = ""; 
        public static string connstr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        public static SqlConnection conn = new SqlConnection();
        public static bool sendmail(string toAdd,string body, string subject)
        {
            bool flag = false;

            try
            {
                //https://www.google.com/settings/security/lesssecureapps
                
                string fromaddId = "";
                string fromaddPass = "";

                string sql = "select * from Email_Configuration";
                DataTable xDt = new DataTable();
                conn = new SqlConnection(connstr);
                SqlDataAdapter dap = new SqlDataAdapter(sql, conn);
                conn.Open();
                dap.Fill(xDt);

                if (xDt.Rows.Count > 0)
                {
                    fromaddId = xDt.Rows[0]["SMTP_User"].ToString();
                    fromaddPass = xDt.Rows[0]["SMTP_Password"].ToString();
                    string Host = xDt.Rows[0]["SMTP_Host"].ToString();
                    int Port = Convert.ToInt32(xDt.Rows[0]["SMTP_Port"].ToString()); 

                    MailMessage mail = new MailMessage();

                    // Gmail Address from where you send the mail
                    string fromAddress = fromaddId;
                    // any address where the email will be sending
                    string toAddress = toAdd;
                    //Password of your gmail address
                    string fromPassword = fromaddPass;
                    // smtp settings
                    SmtpClient smtp = new SmtpClient();
                    {
                        smtp.Host = Host;// "smtp.gmail.com";
                        smtp.Port = Port;// 587;
                        smtp.EnableSsl = true;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                        smtp.Timeout = 20000;
                    }
                    mail.From = new MailAddress(fromaddId);
                    mail.To.Add(toAddress);
                    mail.Subject = subject;
                    mail.Body = body;
                    mail.IsBodyHtml = true;
                    smtp.Send(mail);
                    // Passing values to smtp object
                    //smtp.Send(fromAddress, toAddress, subject, body);

                    flag = true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Dispose();
            }

            return flag;
        }
       
    }
}