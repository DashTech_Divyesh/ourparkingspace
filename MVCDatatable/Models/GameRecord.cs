﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCDatatable.Models
{
    public class GameRecord
    {
        public string Id { get; set; }
        public string LotId { get; set; }
        public string LotName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }

        public int Capacity { get; set; }
        public string DistanceFromVanue { get; set; }

        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public decimal EventPrice { get; set; }
        public decimal RealPrice { get; set; }
        public decimal HourPrice { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string MapLink { get; set; }
        public int Booked { get; set; }
        public int Available { get; set; }

        public string LicensePlate { get; set; }
        public DateTime BookingDate { get; set; }
    }
}