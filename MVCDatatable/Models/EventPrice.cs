﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCDatatable.Models
{
    public class EventPrice
    {
        public int Id { get; set; }
        public double eventPrice { get; set; }
        public string LotName { get; set; }
    }
}