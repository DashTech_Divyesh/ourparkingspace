﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MVCDatatable.Models
{

    public class MyFunctions
    {
        SqlConnection conn = new SqlConnection();
        SqlTransaction transaction;
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataTable dt = new DataTable();

        string connstr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();        
        string sql = "";

         

        public SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        public DataSet DTSET = new DataSet();
        public SqlDataAdapter Adp;

        public int ExecuteWithReturnId(string q)
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand(q, conn);
            //this.transaction = conn.BeginTransaction();
            //cmd.Transaction = transaction;
            int i = 0;
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                SqlCommand CmdGetIdentity = new SqlCommand("Select SCOPE_IDENTITY()", conn);
                i = Convert.ToInt32(CmdGetIdentity.ExecuteScalar());

                //this.transaction.Commit();
            }
            catch(Exception ex)
            {
                throw ex;
                //if (transaction != null) if (transaction.Connection != null) transaction.Rollback();
            }
            finally
            {
                //if (transaction != null) if (transaction.Connection != null) transaction.Dispose();
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Dispose();
                cmd.Dispose();
            }
            return i;
        }

        public int Execute(string q)
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand(q, conn);
            //this.transaction = conn.BeginTransaction();
            //cmd.Transaction = transaction;
            int i = 0;
            try
            {
                conn.Open();
                i = cmd.ExecuteNonQuery();
                //this.transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
                //if (transaction != null) if (transaction.Connection != null) transaction.Rollback();
            }
            finally
            {
                //if (transaction != null) if (transaction.Connection != null) transaction.Dispose();
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Dispose();
                cmd.Dispose();
            }
            return i;
        }
        
        public object Scaler(string q)
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand(q, conn);

            object i = null;
            try
            {
                conn.Open();
                i = cmd.ExecuteScalar();
            }
            catch
            {
            }

            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Dispose();
                cmd.Dispose();
            }
            return i;
        }
        public DataTable GetTable(string q)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connstr);
            SqlDataAdapter dap = new SqlDataAdapter(q, conn);

            try
            {
                conn.Open();
                dap.Fill(dt);
            }
            catch
            {
            }

            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Dispose();
                dap.Dispose();
            }
            return dt;
        }
        public DataSet GetDataSet(string q)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(connstr);
            SqlDataAdapter dap = new SqlDataAdapter(q, conn);

            try
            {
                conn.Open();
                dap.Fill(ds);
            }
            catch
            {
            }

            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Dispose();
                dap.Dispose();
            }
            return ds;
        }

        public string GetUserNameById(string q)
        {
            string UserName = "";
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connstr);
            SqlDataAdapter dap = new SqlDataAdapter(q, conn);

            try
            {
                conn.Open();
                dap.Fill(dt);
                UserName = dt.Rows[0][0].ToString();
            }
            catch
            {
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Dispose();
                dap.Dispose();
            }
            return UserName;
        }

        

        public string GetDeviceTypeById(string q)
        {
            string DeviceType = "";
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connstr);
            SqlDataAdapter dap = new SqlDataAdapter(q, conn);

            try
            {
                conn.Open();
                dap.Fill(dt);
                DeviceType = dt.Rows[0][0].ToString();
            }
            catch
            {
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Dispose();
                dap.Dispose();
            }
            return DeviceType;
        }
        public string GetDeviceTokenById(string q)
        {
            string DeviceToken = "";
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connstr);
            SqlDataAdapter dap = new SqlDataAdapter(q, conn);

            try
            {
                conn.Open();
                dap.Fill(dt);
                DeviceToken = dt.Rows[0][0].ToString();
            }
            catch
            {
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Dispose();
                dap.Dispose();
            }
            return DeviceToken;
        }



        public int Get_NewId(string TBL, string PK)
        {
            String sql = "select max (" + PK + ") from " + TBL + "";
            object c = Scaler(sql);
            int i = 0;
            if (c != null && c.ToString() != "")
            {
                i = int.Parse(c.ToString());
            }
            i = i + 1;
            return i;
        }
        public void FillCombo(DropDownList cbo, string TBL, string Fld_Disp, string Fld_Val, string whr)
        {
            sql = "select " + Fld_Val + ", " + Fld_Disp + " from " + TBL + whr;
            DataTable dt = new DataTable();
            dt = GetTable(sql);

            //cbo.AutoCompleteMode = AutoCompleteMode.Suggest;
            //cbo.AutoCompleteSource = AutoCompleteSource.ListItems;

            cbo.DataTextField = Fld_Disp;
            cbo.DataValueField = Fld_Val;

            DataRow row = dt.NewRow();
            row[Fld_Disp] = "Please Select";
            dt.Rows.InsertAt(row, 0);

            cbo.DataSource = dt;

            if (cbo.Items.Count > 0)
            {
                cbo.SelectedIndex = -1;
            }
            cbo.DataBind();
        }
        public string CreateNewId(string TableName, string FildName, string Char)
        {            
            int MaxCode = 0;
            string Code = "";
            SqlDataAdapter daMax = new SqlDataAdapter("SELECT MAX(["+ FildName + "]) FROM [UserMain]", this.cn);
            DataSet dsMax = new DataSet();
            daMax.Fill(dsMax);
            if (dsMax.Tables[0].Rows[0][0] != DBNull.Value)
            {
                MaxCode = Convert.ToInt32(dsMax.Tables[0].Rows[0][0]);
            }
            MaxCode++;
            if (MaxCode.ToString().Length == 1)
            {
                Code = string.Format(Char + "-000000{0}", MaxCode.ToString());
            }
            else if (MaxCode.ToString().Length == 2)
            {
                Code = string.Format(Char + "-00000{0}", MaxCode.ToString());
            }
            else if (MaxCode.ToString().Length == 3)
            {
                Code = string.Format(Char + "-0000{0}", MaxCode.ToString());
            }
            else if (MaxCode.ToString().Length == 4)
            {

                Code = string.Format(Char + "-000{0}", MaxCode.ToString());
            }
            else if (MaxCode.ToString().Length == 5)
            {
                Code = string.Format(Char + "-00{0}", MaxCode.ToString());
            }
            else if (MaxCode.ToString().Length == 6)
            {
                Code = string.Format(Char + "-0{0}", MaxCode.ToString());
            }
            else
            {
                Code = string.Format(Char + "-{0}", MaxCode.ToString());
            }
            return Code;
        }
        public string GenerateNewId(string TableName, string FildName, string Char)
        {
            string GeneratedID = "";
            String sql = "select MAX (" + FildName + ") from " + TableName + "";
            object c = Scaler(sql);
            string lastID = "";
            if (c != null && c.ToString() != "")
            {
                lastID = c.ToString();
                string Number = lastID.Substring(3, 7);
                int NewNumber = Convert.ToInt32(Number) + 1;
                GeneratedID = Char + NewNumber.ToString("0000000");
            }
            else
            {
                GeneratedID = Char + "0000001";
            }
            return GeneratedID;
        }
        public static string Con()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }
        public static string GetMainBranch(SqlConnection CN, SqlTransaction STMain)
        {
            string BranchCode = string.Empty;
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("Select BranchCode from Branch Where ISMainBranch = 'True'", CN);
            da.SelectCommand.Transaction = STMain;
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0][0] != DBNull.Value)
                {
                    BranchCode = ds.Tables[0].Rows[0][0].ToString();
                }
            }
            return BranchCode;
        }
        public void InsertLogs(String modulename, String username, DateTime date, String transtype, String transdescription, string StrFormTitle, string StrPreviousValue)
        {
            try
            {
                SqlParameter ParaModuleName = new SqlParameter("@ModuleName", modulename);
                SqlParameter ParaUserName = new SqlParameter("@UserName", username);
                SqlParameter ParaDate = new SqlParameter("@Date", date);
                SqlParameter ParaTransType = new SqlParameter("@TransType", transtype);
                SqlParameter ParaTransDescription = new SqlParameter("@TransDescription", transdescription);
                SqlParameter ParaFormTitle = new SqlParameter("@Title", StrFormTitle);
                SqlParameter ParaPreviousValue = new SqlParameter("@PreviousValue", StrPreviousValue);

                SqlConnection CN = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                if (CN.State == ConnectionState.Closed)
                {
                    CN.Open();
                }
                SqlCommand comLog = new SqlCommand("Insert_User_Logs", CN);
                comLog.CommandType = CommandType.StoredProcedure;
                comLog.Parameters.Add(ParaModuleName);
                comLog.Parameters.Add(ParaUserName);
                comLog.Parameters.Add(ParaDate);
                comLog.Parameters.Add(ParaTransType);
                comLog.Parameters.Add(ParaTransDescription);
                comLog.Parameters.Add(ParaFormTitle);
                comLog.Parameters.Add(ParaPreviousValue);
                comLog.ExecuteNonQuery();
                CN.Close();
            }
            catch
            {
            }
        }
        public string GetRowDetails(string StrQueryForData, SqlConnection CN, SqlTransaction STMain, object objFutureUse)
        {
            string StrQueryDetails = string.Empty;
            try
            {
                SqlDataAdapter daQueryDetails = new SqlDataAdapter(StrQueryForData, CN);
                DataSet dsQueryDetails = new DataSet();
                daQueryDetails.SelectCommand.Transaction = STMain;
                daQueryDetails.Fill(dsQueryDetails);
                if (dsQueryDetails.Tables[0].Rows.Count > 0)
                {
                    if (objFutureUse != null)
                    {
                        for (int IntCurrentRow = 0; IntCurrentRow < dsQueryDetails.Tables[0].Rows.Count; IntCurrentRow++)
                        {
                            for (int IntCurrentColumn = 0; IntCurrentColumn < dsQueryDetails.Tables[0].Columns.Count; IntCurrentColumn++)
                            {
                                if (StrQueryDetails != string.Empty)
                                {
                                    StrQueryDetails += ",";
                                }
                                StrQueryDetails += string.Format("{0} = {1}", dsQueryDetails.Tables[0].Columns[IntCurrentColumn].Caption, dsQueryDetails.Tables[0].Rows[IntCurrentRow][IntCurrentColumn].ToString());
                            }
                        }
                    }
                    else
                    {
                        for (int IntCurrentColumn = 0; IntCurrentColumn < dsQueryDetails.Tables[0].Columns.Count; IntCurrentColumn++)
                        {
                            if (StrQueryDetails != string.Empty)
                            {
                                StrQueryDetails += ",";
                            }
                            StrQueryDetails += string.Format("{0} = {1}", dsQueryDetails.Tables[0].Columns[IntCurrentColumn].Caption, dsQueryDetails.Tables[0].Rows[0][IntCurrentColumn].ToString());
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            return StrQueryDetails;
        }

        public string GetRowDetails(string StrQueryForData, object objFutureUse)
        {
            string StrQueryDetails = string.Empty;
            try
            {
                SqlConnection CN = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                SqlDataAdapter daQueryDetails = new SqlDataAdapter(StrQueryForData, CN);
                DataSet dsQueryDetails = new DataSet();
                daQueryDetails.Fill(dsQueryDetails);
                if (dsQueryDetails.Tables[0].Rows.Count > 0)
                {
                    if (objFutureUse != null)
                    {
                        for (int IntCurrentRow = 0; IntCurrentRow < dsQueryDetails.Tables[0].Rows.Count; IntCurrentRow++)
                        {
                            for (int IntCurrentColumn = 0; IntCurrentColumn < dsQueryDetails.Tables[0].Columns.Count; IntCurrentColumn++)
                            {
                                if (StrQueryDetails != string.Empty)
                                {
                                    StrQueryDetails += ",";
                                }
                                StrQueryDetails += string.Format("{0} = {1}", dsQueryDetails.Tables[0].Columns[IntCurrentColumn].Caption, dsQueryDetails.Tables[0].Rows[IntCurrentRow][IntCurrentColumn].ToString());
                            }
                        }
                    }
                    else
                    {
                        for (int IntCurrentColumn = 0; IntCurrentColumn < dsQueryDetails.Tables[0].Columns.Count; IntCurrentColumn++)
                        {
                            if (StrQueryDetails != string.Empty)
                            {
                                StrQueryDetails += ",";
                            }
                            StrQueryDetails += string.Format("{0} = {1}", dsQueryDetails.Tables[0].Columns[IntCurrentColumn].Caption, dsQueryDetails.Tables[0].Rows[0][IntCurrentColumn].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string s = string.Format("ERROR : {0}", ex.Message);
            }
            return StrQueryDetails;
        }
        private string Encrypt(string clearText)
        {
            string EncryptionKey = "IMSV2OPIXA11162";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }




        public DataTable GetFormsList()
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connstr);
            SqlDataAdapter dap = new SqlDataAdapter("Select * from FormsMaster where Status='True' order by Sequence", conn);
            try
            {
                conn.Open();
                dap.Fill(dt);
            }
            catch
            {
            }

            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Dispose();
                dap.Dispose();
            }
            return dt;
        }


        public string GetParentUserNameById(int UserID)
        {
            string UserName = "";
            String sql = "select Username from UserMaster where Id = " + UserID;
            object c = Scaler(sql);
            if (c != null && c.ToString() != "")
            {
                UserName = c.ToString();
            }
            else
            {
                UserName = "-";
            }
            return UserName;
        }

        public int GetParentUserId(int UserID)
        {
            int ParentUserId = 0;
            String sql = "select ParentUserId from UserMaster where Id = " + UserID;
            object c = Scaler(sql);
            if (c != null && c.ToString() != "")
            {
                ParentUserId = Convert.ToInt32(c.ToString());
            }
            else
            {
                ParentUserId = 0;
            }
            return ParentUserId;
        }

    }
}