﻿namespace MVCDatatable.Models
{
    public class EventParkingLotView
    {
        public int Id { get; set; }
        public string EventName { get; set; }
        public string LotId { get; set; }
        public string LotName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public int Capacity { get; set; }
        public string DistanceFromVanue { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string EventPrice { get; set; }
        public string HourPrice { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string MapLink { get; set; }
        public string Status { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public int Booked { get; set; }
        public int Available { get; set; }
    }
}