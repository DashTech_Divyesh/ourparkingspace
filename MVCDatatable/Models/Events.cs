﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVCDatatable.Models
{
    public class Events
    {
        public int Id { get; set; }
        public string LotId { get; set; }
        public string EventName { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Prize { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }

        public virtual ParkingLot ParkingLots { get; set; }
    }
}