﻿using Exceptionless;
using MVCDatatable.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;

namespace MVCDatatable.Controllers
{
    public class TowingController : Controller
    {
        Models.ReturnMSG R1 = new Models.ReturnMSG();
        Models.MyFunctions my = new Models.MyFunctions();

        public ActionResult Index()
        {
            if (Session["UserName"] != null && Session["Password"] != null && Session["UserId"] != null && Session["UserLotId"] != null && (string)Session["UserName"] == "TowAdmin")
            {
                int uid = Convert.ToInt32(Session["UserId"]);
                string ulotId = Session["UserLotId"].ToString();
                GetParkingLotWithUser(uid, ulotId);
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Towing");
            }
        }

        public ActionResult Login(TowingLogin user)
        {
            ViewBag.Message = null;
            Session["UserName"] = null;
            Session["Password"] = null;

            if (ModelState.IsValid)
            {
                if (user.IsValid(user.UserName, user.Password))
                {
                    using (SqlConnection CN = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        string username = user.UserName.ToString();
                        string password = user.Password.ToString();
                        if (user.RememberMe)
                        {
                            var cookie = FormsAuthentication.GetAuthCookie(user.UserName, user.RememberMe);
                            cookie.Expires = DateTime.Now.AddDays(10);
                            Response.Cookies.Add(cookie);
                        }
                        string sql = "Select [Id],[LotId] From [dbo].[Users] Where [Username] = '" + username + "' and [Password] = '" + password + "' ";
                        var cmd = new SqlCommand(sql, CN);
                        CN.Open();
                        DataTable xDt = new DataTable();
                        xDt = my.GetTable(sql);

                        if (xDt.Rows.Count > 0)
                        {
                            UserLogin ul = new UserLogin();
                            ul.Id = Convert.ToInt32(xDt.Rows[0]["Id"]);
                            ul.LotId = xDt.Rows[0]["LotId"].ToString();
                            Session["UserId"] = ul.Id;
                            Session["UserLotId"] = ul.LotId;
                        }
                        CN.Close();
                    }
                    Session["UserName"] = user.UserName;
                    Session["Password"] = user.Password;
                    return RedirectToAction("GetParkingLotWithUser", new { Id = Session["UserId"], UserLotId = Session["UserLotId"] });
                }
                else
                {
                    ViewBag.Message = "Either UserName or Password Incorrect";
                }
            }
            return View(user);
        }

        public ActionResult GetParkingLotWithUser(int Id, string userLotId)
        {
            try
            {
                if(Session["UserId"] != null && Session["UserLotId"] != null)
                {
                    string uLotId = userLotId;
                    int EventId = int.Parse(WebConfigurationManager.AppSettings["EventId"]);
                    string sql = "Select PL.LotName, PL.Address, BM.LicensePlate, BM.BookingDate from BookingMaster BM, ParkingLot PL, Events ET Where BM.LotId = PL.Id And BM.EventId = ET.Id And ET.id = " + EventId + " and BM.LotId In(" + uLotId + ") order by[BookingDate] desc";
                    DataTable xDt = new DataTable();
                    xDt = my.GetTable(sql);

                    List<GetParkingData> parking = new List<GetParkingData>();
                    if (xDt.Rows.Count > 0)
                    {
                        for (int i = 0; i < xDt.Rows.Count; i++)
                        {
                            GetParkingData dmt2 = new GetParkingData();

                            dmt2.LotName = xDt.Rows[i]["LotName"].ToString();
                            dmt2.Address = xDt.Rows[i]["Address"].ToString();
                            dmt2.LicensePlate = xDt.Rows[i]["LicensePlate"].ToString();
                            dmt2.BookingDate = Convert.ToDateTime(xDt.Rows[i]["BookingDate"].ToString());
                            parking.Add(dmt2);
                        }
                    }

                    DataTable xTotalCount = new DataTable();
                    xTotalCount = my.GetTable(sql);

                    ViewBag.ParkingDataList = parking;

                    R1.Status = 1;
                    R1.Message = "Successfully.";
                }
                else
                {
                    return RedirectToAction("Login", "Towing");
                }
                ExceptionlessClient.Default.SubmitLog("Get Parking Data");
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return View("Index");
        }

        [HttpPost]
        public JsonResult GetParkingLotName()
        {
            try
            {
                string sql = "Select Id,LotName from [dbo].[ParkingLot] order By [DistanceFromVanue] desc";
                DataTable xDt = new DataTable();
                xDt = my.GetTable(sql);

                List<Parkinglot> lotlist = new List<Parkinglot>();
                if (xDt.Rows.Count > 0)
                {
                    for (int i = 0; i < xDt.Rows.Count; i++)
                    {
                        Parkinglot parkinglotList = new Parkinglot();
                        parkinglotList.ParkingLotId = Convert.ToInt32(xDt.Rows[i]["Id"]);
                        parkinglotList.ParkingLotName = xDt.Rows[i]["LotName"].ToString();

                        lotlist.Add(parkinglotList);
                    }
                }
                return Json(lotlist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRecordsByParkingLotName(int Id)
        {
            try
            {
                int EventId = int.Parse(WebConfigurationManager.AppSettings["EventId"]);
                string userlotId = Session["UserLotId"].ToString();
                string sql = "Select PL.LotName, PL.Address, BM.LicensePlate, BM.BookingDate from BookingMaster BM, ParkingLot PL, Events ET Where BM.LotId = PL.Id And BM.EventId = ET.Id And ET.id = " + EventId + " and BM.LotId In(" + userlotId + ") and PL.Id = " + Id + " order by[BookingDate] desc";
                if (Id == 0)
                {
                    sql = "Select PL.LotName, PL.Address, BM.LicensePlate, BM.BookingDate from BookingMaster BM, ParkingLot PL, Events ET Where BM.LotId = PL.Id And BM.EventId = ET.Id And ET.id = " + EventId + " and BM.LotId In(" + userlotId + ") order by[BookingDate] desc";
                }
                DataTable xDt = new DataTable();
                xDt = my.GetTable(sql);

                List<GameRecord> game = new List<GameRecord>();
                if (xDt.Rows.Count > 0)
                {
                    for (int i = 0; i < xDt.Rows.Count; i++)
                    {
                        GameRecord gmr = new GameRecord();

                        gmr.LotName = xDt.Rows[i]["LotName"].ToString();
                        gmr.Address = xDt.Rows[i]["Address"].ToString();
                        gmr.LicensePlate = xDt.Rows[i]["LicensePlate"].ToString();
                        DateTime dte = Convert.ToDateTime(xDt.Rows[i]["BookingDate"].ToString());
                        DateTime ut = DateTime.SpecifyKind(dte, DateTimeKind.Utc);
                        var timeUtc = ut;
                        TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                        DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);
                        gmr.BookingDate = easternTime;
                        game.Add(gmr);
                    }
                }

                DataTable xTotalCount = new DataTable();
                xTotalCount = my.GetTable(sql);

                ViewBag.ParkinglotRecord = game;

                R1.Status = 1;
                R1.Message = "Successfully.";
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return PartialView("GetRecordsByParkingLotName");
        }

        public ActionResult Logout()
        {
            Session["UserName"] = null;
            Session["Password"] = null;
            Session["UserId"] = null;
            Session["UserLotId"] = null;
            return RedirectToAction("Login", "Towing");
        }

        public JsonResult GetParkingData()
        {
            try
            {
                int EventId = int.Parse(WebConfigurationManager.AppSettings["EventId"]);
                string sql = "Select PL.LotName, PL.Address, BM.LicensePlate, BM.BookingDate from BookingMaster BM, ParkingLot PL, Events ET Where BM.LotId=PL.Id And BM.EventId=ET.Id and ET.id = "+ EventId +" order by [BookingDate] desc";
                DataTable xDt = new DataTable();
                xDt = my.GetTable(sql);

                List<GetParkingData> parking = new List<GetParkingData>();
                if (xDt.Rows.Count > 0)
                {
                    for (int i = 0; i < xDt.Rows.Count; i++)
                    {
                        GetParkingData dmt2 = new GetParkingData();

                        dmt2.LotName = xDt.Rows[i]["LotName"].ToString();
                        dmt2.Address = xDt.Rows[i]["Address"].ToString();
                        dmt2.LicensePlate = xDt.Rows[i]["LicensePlate"].ToString();
                        dmt2.BookingDate = Convert.ToDateTime(xDt.Rows[i]["BookingDate"].ToString());
                        parking.Add(dmt2);
                    }
                }

                DataTable xTotalCount = new DataTable();
                xTotalCount = my.GetTable(sql);

                ViewBag.ParkingDataList = parking;

                R1.Status = 1;
                R1.Message = "Successfully.";

                ExceptionlessClient.Default.SubmitLog("Get Parking Data");
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ContactUs()
        {
            return View();
        }
    }
}
