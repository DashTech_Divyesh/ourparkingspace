﻿using Exceptionless;
using MVCDatatable.Models;
using Stripe;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Web.Configuration;
using System.Web.Mvc;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace MVCDatatable.Controllers
{
    
    public class Stripe
    {
        public string ProcessTransaction(string Key, string cc_number, string cc_month, string cc_year, string cc_name, string csv, decimal amount, string email, string mobile)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string response = "";
            try
            {
                decimal chargetotal = amount;
                chargetotal = chargetotal * 100;

                StripeConfiguration.SetApiKey(Key);

                var tokenOptions = new StripeTokenCreateOptions()
                {
                    Card = new StripeCreditCardOptions()
                    {
                        Number = cc_number,
                        ExpirationYear = Convert.ToInt32(cc_year),
                        ExpirationMonth = Convert.ToInt32(cc_month),
                        Cvc = csv,
                        Name = cc_name,

                    }
                };
                var toakenService = new StripeTokenService();
                StripeToken stripeToken = toakenService.Create(tokenOptions);
                string sourcetoken = stripeToken.Id;



                var customers = new StripeCustomerService();
                var customer = customers.Create(new StripeCustomerCreateOptions
                {
                    Email = email,
                    SourceToken = sourcetoken
                });

                var chargeOptions = new StripeChargeCreateOptions()
                {
                    Amount = Convert.ToInt32(Math.Round(chargetotal)),
                    Currency = "usd",
                    ReceiptEmail = email,
                    Description = "OurParking.Space Charge",
                    CustomerId = customer.Id

                };
                var chargeService = new StripeChargeService();
                StripeCharge charge = chargeService.Create(chargeOptions);
                response = charge.Id;
                response = charge.Status;
                response = charge.BalanceTransactionId;
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog("Payment Failed");
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
                response = (ex.Message);
                response = "";
            }
            return response;
        }

        public string AdminBookingProcessTransaction(string Key, decimal amount, string email, string mobile)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string response = "";
            try
            {
                decimal chargetotal = amount;
                chargetotal = chargetotal * 100;

                StripeConfiguration.SetApiKey(Key);

                Random generator = new Random();
                String token = generator.Next(0, 999999).ToString("D6");
                
                string sourcetoken = token;

                var customers = new Customers();
                var customer = new Customers
                {
                    Email = email,
                    SourceToken = sourcetoken
                };
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog("Payment Failed");
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
                response = (ex.Message);
                response = "";
            }
            return response;
        }
    }

    public class BookingController : Controller
    {
        string CN = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        Models.ReturnMSG R1 = new Models.ReturnMSG();
        Models.MyFunctions my = new Models.MyFunctions();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Bundle()
        {
            return View();
        }

        public ActionResult MakeBooking(BookingMaster bm)
        {
            ExceptionlessClient.Default.SubmitLog("Booking Started");

            try
            {
                string StripKey = WebConfigurationManager.AppSettings["StripKey"];
                int EventId = int.Parse(WebConfigurationManager.AppSettings["EventId"]);
                string BookingCode = "";
                string xMobile = "";

                string MobileNo = "+1" + bm.MobileNo;
                xMobile = MobileNo;
                bm.EventId = EventId;

                string Email = bm.Email;
                string _Transaction_Number = "-";

                Stripe _stripe = new Stripe();
                string stripe_result = _stripe.ProcessTransaction(StripKey, bm.Extra1, bm.Extra2, bm.Extra3, bm.Extra4, bm.Extra5, Convert.ToDecimal(bm.Price), Email, xMobile);

                if (!String.IsNullOrEmpty(stripe_result))
                {
                    _Transaction_Number = stripe_result;

                    int BookingId = 0;

                    int CustId = my.ExecuteWithReturnId("insert into Customer(CustomerName,Email,Mobile)values('" + bm.CustomerName + "','" + bm.Email + "','" + xMobile + "')");
                    if (CustId > 0)
                    {
                        var timeUtc = DateTime.UtcNow;
                        TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                        DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);

                        BookingId = my.ExecuteWithReturnId("insert into BookingMaster(BookingDate,LotId,CustomerName,CustomerId,EventId,LicensePlate,Price,TransactionNumber,Email,MobileNo)values('" + easternTime + "'," + bm.LotId + ",'" + bm.CustomerName + "'," + CustId + ",'"+ bm.EventId +"','" + bm.LicensePlate + "'," + bm.Price + ",'" + _Transaction_Number + "','" + bm.Email + "','" + xMobile + "')");
                        if (BookingId > 0)
                        {
                            //================================Generate Booking Code==================================
                            int MaxCode = BookingId;
                            if (MaxCode.ToString().Length == 1)
                            {
                                BookingCode = string.Format("BKN-000000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 2)
                            {
                                BookingCode = string.Format("BKN-00000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 3)
                            {
                                BookingCode = string.Format("BKN-0000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 4)
                            {

                                BookingCode = string.Format("BKN-000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 5)
                            {
                                BookingCode = string.Format("BKN-00{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 6)
                            {
                                BookingCode = string.Format("BKN-0{0}", MaxCode.ToString());
                            }
                            else
                            {
                                BookingCode = string.Format("BKN-{0}", MaxCode.ToString());
                            }
                            my.Execute("Update BookingMaster set BookingCode='" + BookingCode + "' where Id = " + BookingId);
                            //==================================================================================

                            ExceptionlessClient.Default.SubmitLog("Payment Success");
                            if(WebConfigurationManager.AppSettings["Environment"] == "live")
                            {
                                EmailSend(BookingId);
                                SendText(BookingId);
                            }
                            
                            ExceptionlessClient.Default.SubmitLog("Booking Successfull");
                        }
                    }

                    R1.Status = 1;
                    R1.Message = BookingId.ToString();
                }
                else
                {
                    _Transaction_Number = "Failed";
                    R1.Status = 0;
                    R1.Message = stripe_result;
                }
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog("Booking Failed");                
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }

            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BookAllLots(BookingMaster bm)
        {
            ExceptionlessClient.Default.SubmitLog("Booking Started");
            try
            {
                string StripKey = WebConfigurationManager.AppSettings["StripKey"];
                int EventId = int.Parse(WebConfigurationManager.AppSettings["EventId"]);
                string BookingCode = "";
                string xMobile = "";

                string MobileNo = "+1" + bm.MobileNo;
                xMobile = MobileNo;
                bm.EventId = EventId;

                string Email = bm.Email;
                string _Transaction_Number = "-";

                Stripe _stripe = new Stripe();
                string stripe_result = _stripe.ProcessTransaction(StripKey, bm.Extra1, bm.Extra2, bm.Extra3, bm.Extra4, bm.Extra5, Convert.ToDecimal(bm.Price), Email, xMobile);
                if (!String.IsNullOrEmpty(stripe_result))
                {
                    _Transaction_Number = stripe_result;
                    int BookingId = 0;
                    int CustId = my.ExecuteWithReturnId("insert into Customer(CustomerName,Email,Mobile)values('" + bm.CustomerName + "','" + bm.Email + "','" + xMobile + "')");
                    if (CustId > 0)
                    {
                        var timeUtc = DateTime.UtcNow;
                        TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                        DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);

                        BookingId = my.ExecuteWithReturnId("insert into BookingMaster(BookingDate,LotId,CustomerName,CustomerId,EventId,LicensePlate,Price,TransactionNumber,Email,MobileNo)values('" + easternTime + "'," + bm.LotId + ",'" + bm.CustomerName + "'," + CustId + ",'" + bm.EventId + "','" + bm.LicensePlate + "'," + bm.Price + ",'" + _Transaction_Number + "','" + bm.Email + "','" + xMobile + "')");
                        if (BookingId > 0)
                        {
                            //================================Generate Booking Code==================================
                            int MaxCode = BookingId;
                            if (MaxCode.ToString().Length == 1)
                            {
                                BookingCode = string.Format("BKN-000000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 2)
                            {
                                BookingCode = string.Format("BKN-00000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 3)
                            {
                                BookingCode = string.Format("BKN-0000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 4)
                            {

                                BookingCode = string.Format("BKN-000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 5)
                            {
                                BookingCode = string.Format("BKN-00{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 6)
                            {
                                BookingCode = string.Format("BKN-0{0}", MaxCode.ToString());
                            }
                            else
                            {
                                BookingCode = string.Format("BKN-{0}", MaxCode.ToString());
                            }
                            my.Execute("Update BookingMaster set BookingCode='" + BookingCode + "' where Id = " + BookingId);
                            //==================================================================================

                            ExceptionlessClient.Default.SubmitLog("Payment Success");
                            if (WebConfigurationManager.AppSettings["Environment"] == "live")
                            {
                                EmailSend(BookingId);
                                SendText(BookingId);
                            }

                            ExceptionlessClient.Default.SubmitLog("Booking Successfull");
                        }
                    }

                    R1.Status = 1;
                    R1.Message = BookingId.ToString();
                }
                else
                {
                    _Transaction_Number = "Failed";
                    R1.Status = 0;
                    R1.Message = stripe_result;
                }
            }
            catch(Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog("Booking Failed");
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public bool SendEmail(string CustomerName, string CustomerEmail, string BookingDate, string Price, string TransactionNumber,string BookingCode, string LotName, string Address, string StartTime, string EndTime, string MapLink)
        {
            string body = PopulateBody(CustomerName, BookingDate, Price, TransactionNumber, BookingCode, LotName, Address, StartTime, EndTime, MapLink);
            bool flag = Mail.sendmail(CustomerEmail, body, "Our Parking Invoice");
            return flag;
        }

        public string PopulateBody(string CustomerName, string BookingDate, string Price, string TransactionNumber, string BookingCode, string LotName, string Address, string StartTime, string EndTime, string MapLink)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Emailbody.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{CustomerName}", CustomerName);
            body = body.Replace("{BookingDate}", BookingDate);
            body = body.Replace("{Price}", Price);
            body = body.Replace("{TransactionNumber}", TransactionNumber);
            body = body.Replace("{BookingCode}", BookingCode); 
            body = body.Replace("{LotName}", LotName);
            body = body.Replace("{Address}", Address);
            body = body.Replace("{StartTime}", StartTime);
            body = body.Replace("{EndTime}", EndTime);
            body = body.Replace("{MapLink}", MapLink);
            return body;
        }

        public JsonResult EmailSend(int ID)
        {
            try
            {
                string sql = "select * from BookingMaster where Id=" + ID;
                DataTable dt = new DataTable();
                dt = my.GetTable(sql);

                if (dt.Rows.Count > 0)
                {
                    DataTable xDt = new DataTable();
                    xDt = my.GetTable("select * from ParkingLot where Id = " + Convert.ToInt32(dt.Rows[0]["LotId"].ToString()));
                    string LotName = xDt.Rows[0]["LotName"].ToString();
                    string Address = xDt.Rows[0]["Address"].ToString() + ',' + xDt.Rows[0]["City"].ToString() + ',' + xDt.Rows[0]["State"].ToString() + ',' + xDt.Rows[0]["Country"].ToString() + ' ' + xDt.Rows[0]["ZipCode"].ToString();
                    string StartTime = xDt.Rows[0]["StartTime"].ToString();
                    string EndTime = xDt.Rows[0]["EndTime"].ToString();
                    string MapLink = xDt.Rows[0]["MapLink"].ToString();

                    var timeUtc = DateTime.UtcNow;
                    TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);

                    bool flag = SendEmail(dt.Rows[0]["CustomerName"].ToString(), dt.Rows[0]["Email"].ToString(), easternTime.ToString(), Convert.ToString(Math.Round(Convert.ToDecimal(dt.Rows[0]["Price"].ToString()), 2)).ToString(), dt.Rows[0]["TransactionNumber"].ToString(), dt.Rows[0]["BookingCode"].ToString(), LotName, Address, StartTime, EndTime, MapLink);

                    R1.Status = 1;
                    R1.Message = "E-Mail Send Successfully.";

                    ExceptionlessClient.Default.SubmitLog("Mail Send");
                }
                else
                {
                    R1.Status = 0;
                    R1.Message = "E-Mail Not Send.";
                }
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog("Mail Failed");
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }

            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SendText(int ID)
        {
            try
            {
                string sql = "select * from BookingMaster where Id=" + ID;
                DataTable dt = new DataTable();
                dt = my.GetTable(sql);
                if (dt.Rows.Count > 0)
                {
                    string MobileNo = dt.Rows[0]["MobileNo"].ToString();
                    string BookingCode = dt.Rows[0]["BookingCode"].ToString();

                    DataTable xDt = new DataTable();
                    xDt = my.GetTable("select * from ParkingLot where Id = " + Convert.ToInt32(dt.Rows[0]["LotId"].ToString()));
                    string LotName = xDt.Rows[0]["LotName"].ToString();
                    string Address = xDt.Rows[0]["Address"].ToString() + ',' + xDt.Rows[0]["City"].ToString() + ',' + xDt.Rows[0]["State"].ToString() + ',' + xDt.Rows[0]["Country"].ToString() + ' ' + xDt.Rows[0]["ZipCode"].ToString();
                    string StartTime = xDt.Rows[0]["StartTime"].ToString();
                    string EndTime = xDt.Rows[0]["EndTime"].ToString();
                    string MapLink = xDt.Rows[0]["MapLink"].ToString();

                    var timeUtc = DateTime.UtcNow;
                    TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);

                    string SMS = "Your booking for " + LotName + " done on " + easternTime.ToString() + " successfully, booking Number is " + BookingCode + " park your vehicle at " + Address + ", Thanks for using our parking service.";

                    string accountSid = WebConfigurationManager.AppSettings["TaccountSid"]; //"ACadd23611845024e899ed8c52d213853f";
                    string authToken = WebConfigurationManager.AppSettings["TauthToken"];   //"68b6e0fd76acc3e01e4d3704e95041da";
                    string TMobileNo = WebConfigurationManager.AppSettings["TMobile"];   //"+16144124088";
                    TwilioClient.Init(accountSid, authToken);
                    var message = MessageResource.Create(
                        body: SMS,
                        from: new Twilio.Types.PhoneNumber(TMobileNo),
                        to: new Twilio.Types.PhoneNumber(MobileNo)
                    );

                    R1.Status = 1;
                    R1.Message = "E-Mail Send Successfully.";

                    ExceptionlessClient.Default.SubmitLog("Text Send");
                }
                else
                {
                    R1.Status = 0;
                    R1.Message = "E-Mail Not Send.";
                }
            }
            catch(Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog("Text Failed");
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public void LogError(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            string path = Server.MapPath("~/ErrorLog/ErrorLog.txt");
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }
    }

    public class AdminBookingController : Controller
    {
        string CN = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        Models.ReturnMSG R1 = new Models.ReturnMSG();
        Models.MyFunctions my = new Models.MyFunctions();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MakeBooking(BookingMaster bm)
        {
            ExceptionlessClient.Default.SubmitLog("Admin Booking Started");

            try
            {
                string StripKey = WebConfigurationManager.AppSettings["StripKey"];
                int EventId = int.Parse(WebConfigurationManager.AppSettings["EventId"]);
                string BookingCode = "";
                string xMobile = "";

                string MobileNo = "+1" + bm.MobileNo;
                xMobile = MobileNo;
                bm.EventId = EventId;

                string Email = bm.Email;
                string _Transaction_Number = "-";

                Stripe _stripe = new Stripe();
                string stripe_result = _stripe.ProcessTransaction(StripKey, bm.Extra1, bm.Extra2, bm.Extra3, bm.Extra4, bm.Extra5, Convert.ToDecimal(bm.Price), Email, xMobile);

                if (!String.IsNullOrEmpty(stripe_result))
                {
                    _Transaction_Number = stripe_result;

                    int BookingId = 0;

                    int CustId = my.ExecuteWithReturnId("insert into Customer(CustomerName,Email,Mobile)values('" + bm.CustomerName + "','" + bm.Email + "','" + xMobile + "')");
                    if (CustId > 0)
                    {
                        var timeUtc = DateTime.UtcNow;
                        TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                        DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);

                        BookingId = my.ExecuteWithReturnId("insert into BookingMaster(BookingDate,LotId,CustomerName,CustomerId,EventId,LicensePlate,Price,TransactionNumber,Email,MobileNo)values('" + easternTime + "'," + bm.LotId + ",'" + bm.CustomerName + "'," + CustId + ",'" + bm.EventId + "','" + bm.LicensePlate + "'," + bm.Price + ",'" + _Transaction_Number + "','" + bm.Email + "','" + xMobile + "')");
                        if (BookingId > 0)
                        {
                            //================================Generate Booking Code==================================
                            int MaxCode = BookingId;
                            if (MaxCode.ToString().Length == 1)
                            {
                                BookingCode = string.Format("BKN-000000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 2)
                            {
                                BookingCode = string.Format("BKN-00000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 3)
                            {
                                BookingCode = string.Format("BKN-0000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 4)
                            {

                                BookingCode = string.Format("BKN-000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 5)
                            {
                                BookingCode = string.Format("BKN-00{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 6)
                            {
                                BookingCode = string.Format("BKN-0{0}", MaxCode.ToString());
                            }
                            else
                            {
                                BookingCode = string.Format("BKN-{0}", MaxCode.ToString());
                            }
                            my.Execute("Update BookingMaster set BookingCode='" + BookingCode + "' where Id = " + BookingId);
                            //==================================================================================

                            ExceptionlessClient.Default.SubmitLog("Payment Success");

                            if (WebConfigurationManager.AppSettings["Environment"] == "live")
                            {
                                EmailSend(BookingId);
                                SendText(BookingId);
                            }

                            ExceptionlessClient.Default.SubmitLog("Booking Successfull");
                        }
                    }

                    R1.Status = 1;
                    R1.Message = BookingId.ToString();
                }
                else
                {
                    _Transaction_Number = "Failed";
                    R1.Status = 0;
                    R1.Message = stripe_result;
                }
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog("Booking Failed");
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }

            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MakeBookingViaCash(BookingMaster bm)
        {
            ExceptionlessClient.Default.SubmitLog("Admin Cash Booking Started");
            try
            {
                string StripKey = WebConfigurationManager.AppSettings["StripKey"];
                int EventId = int.Parse(WebConfigurationManager.AppSettings["EventId"]);
                string BookingCode = "";
                string xMobile = "";

                string MobileNo = "+1" + bm.MobileNo;
                xMobile = MobileNo;
                bm.EventId = EventId;

                string Email = bm.Email;
                string _Transaction_Number = "-";

                Stripe _stripe = new Stripe();
                string stripe_result = _stripe.AdminBookingProcessTransaction(StripKey, Convert.ToDecimal(bm.Price), Email, xMobile);
                if (String.IsNullOrEmpty(stripe_result))
                {
                    Random generator = new Random();
                    String token = generator.Next(0, 999999).ToString("D6");
                    stripe_result = "txn_" + token;
                }

                if (!String.IsNullOrEmpty(stripe_result))
                {
                    _Transaction_Number = stripe_result;

                    int BookingId = 0;

                    int CustId = my.ExecuteWithReturnId("insert into Customer(CustomerName,Email,Mobile)values('" + bm.CustomerName + "','" + bm.Email + "','" + xMobile + "')");
                    if (CustId > 0)
                    {
                        var timeUtc = DateTime.UtcNow;
                        TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                        DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);

                        BookingId = my.ExecuteWithReturnId("insert into BookingMaster(BookingDate,LotId,CustomerName,CustomerId,EventId,LicensePlate,Price,TransactionNumber,Email,MobileNo)values('" + easternTime + "'," + bm.LotId + ",'" + bm.CustomerName + "'," + CustId + ",'" + bm.EventId + "','" + bm.LicensePlate + "'," + bm.Price + ",'" + _Transaction_Number + "','" + bm.Email + "','" + xMobile + "')");
                        if (BookingId > 0)
                        {
                            //================================Generate Booking Code==================================
                            int MaxCode = BookingId;
                            if (MaxCode.ToString().Length == 1)
                            {
                                BookingCode = string.Format("BKN-000000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 2)
                            {
                                BookingCode = string.Format("BKN-00000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 3)
                            {
                                BookingCode = string.Format("BKN-0000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 4)
                            {

                                BookingCode = string.Format("BKN-000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 5)
                            {
                                BookingCode = string.Format("BKN-00{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 6)
                            {
                                BookingCode = string.Format("BKN-0{0}", MaxCode.ToString());
                            }
                            else
                            {
                                BookingCode = string.Format("BKN-{0}", MaxCode.ToString());
                            }
                            my.Execute("Update BookingMaster set BookingCode='" + BookingCode + "' where Id = " + BookingId);
                            //==================================================================================

                            ExceptionlessClient.Default.SubmitLog("Payment Success");

                            if (WebConfigurationManager.AppSettings["Environment"] == "live")
                            {
                                EmailSend(BookingId);
                                SendText(BookingId);
                            }

                            ExceptionlessClient.Default.SubmitLog("Booking Successfull");
                        }
                    }

                    R1.Status = 1;
                    R1.Message = BookingId.ToString();
                }
                else
                {
                    _Transaction_Number = "Failed";
                    R1.Status = 0;
                    R1.Message = stripe_result;
                }
            }
            catch(Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog("Admin Cash Booking Failed");
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public bool SendEmail(string CustomerName, string CustomerEmail, string BookingDate, string Price, string TransactionNumber, string BookingCode, string LotName, string Address, string StartTime, string EndTime, string MapLink)
        {
            string body = PopulateBody(CustomerName, BookingDate, Price, TransactionNumber, BookingCode, LotName, Address, StartTime, EndTime, MapLink);
            bool flag = Mail.sendmail(CustomerEmail, body, "Our Parking Invoice");
            return flag;
        }

        public string PopulateBody(string CustomerName, string BookingDate, string Price, string TransactionNumber, string BookingCode, string LotName, string Address, string StartTime, string EndTime, string MapLink)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Emailbody.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{CustomerName}", CustomerName);
            body = body.Replace("{BookingDate}", BookingDate);
            body = body.Replace("{Price}", Price);
            body = body.Replace("{TransactionNumber}", TransactionNumber);
            body = body.Replace("{BookingCode}", BookingCode);
            body = body.Replace("{LotName}", LotName);
            body = body.Replace("{Address}", Address);
            body = body.Replace("{StartTime}", StartTime);
            body = body.Replace("{EndTime}", EndTime);
            body = body.Replace("{MapLink}", MapLink);
            return body;
        }

        public JsonResult EmailSend(int ID)
        {
            try
            {
                string sql = "select * from BookingMaster where Id=" + ID;
                DataTable dt = new DataTable();
                dt = my.GetTable(sql);

                if (dt.Rows.Count > 0)
                {
                    DataTable xDt = new DataTable();
                    xDt = my.GetTable("select * from ParkingLot where Id = " + Convert.ToInt32(dt.Rows[0]["LotId"].ToString()));
                    string LotName = xDt.Rows[0]["LotName"].ToString();
                    string Address = xDt.Rows[0]["Address"].ToString() + ',' + xDt.Rows[0]["City"].ToString() + ',' + xDt.Rows[0]["State"].ToString() + ',' + xDt.Rows[0]["Country"].ToString() + ' ' + xDt.Rows[0]["ZipCode"].ToString();
                    string StartTime = xDt.Rows[0]["StartTime"].ToString();
                    string EndTime = xDt.Rows[0]["EndTime"].ToString();
                    string MapLink = xDt.Rows[0]["MapLink"].ToString();

                    var timeUtc = DateTime.UtcNow;
                    TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);

                    bool flag = SendEmail(dt.Rows[0]["CustomerName"].ToString(), dt.Rows[0]["Email"].ToString(), easternTime.ToString(), Convert.ToString(Math.Round(Convert.ToDecimal(dt.Rows[0]["Price"].ToString()), 2)).ToString(), dt.Rows[0]["TransactionNumber"].ToString(), dt.Rows[0]["BookingCode"].ToString(), LotName, Address, StartTime, EndTime, MapLink);

                    R1.Status = 1;
                    R1.Message = "E-Mail Send Successfully.";

                    ExceptionlessClient.Default.SubmitLog("Mail Send");
                }
                else
                {
                    R1.Status = 0;
                    R1.Message = "E-Mail Not Send.";
                }
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog("Mail Failed");
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }

            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SendText(int ID)
        {
            try
            {
                string sql = "select * from BookingMaster where Id=" + ID;
                DataTable dt = new DataTable();
                dt = my.GetTable(sql);
                if (dt.Rows.Count > 0)
                {
                    string MobileNo = dt.Rows[0]["MobileNo"].ToString();
                    string BookingCode = dt.Rows[0]["BookingCode"].ToString();

                    DataTable xDt = new DataTable();
                    xDt = my.GetTable("select * from ParkingLot where Id = " + Convert.ToInt32(dt.Rows[0]["LotId"].ToString()));
                    string LotName = xDt.Rows[0]["LotName"].ToString();
                    string Address = xDt.Rows[0]["Address"].ToString() + ',' + xDt.Rows[0]["City"].ToString() + ',' + xDt.Rows[0]["State"].ToString() + ',' + xDt.Rows[0]["Country"].ToString() + ' ' + xDt.Rows[0]["ZipCode"].ToString();
                    string StartTime = xDt.Rows[0]["StartTime"].ToString();
                    string EndTime = xDt.Rows[0]["EndTime"].ToString();
                    string MapLink = xDt.Rows[0]["MapLink"].ToString();

                    var timeUtc = DateTime.UtcNow;
                    TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);

                    string SMS = "Your booking for " + LotName + " done on " + easternTime.ToString() + " successfully, booking Number is " + BookingCode + " park your vehicle at " + Address + ", Thanks for using our parking service.";

                    string accountSid = WebConfigurationManager.AppSettings["TaccountSid"]; //"ACadd23611845024e899ed8c52d213853f";
                    string authToken = WebConfigurationManager.AppSettings["TauthToken"];   //"68b6e0fd76acc3e01e4d3704e95041da";
                    string TMobileNo = WebConfigurationManager.AppSettings["TMobile"];   //"+16144124088";
                    TwilioClient.Init(accountSid, authToken);
                    var message = MessageResource.Create(
                        body: SMS,
                        from: new Twilio.Types.PhoneNumber(TMobileNo),
                        to: new Twilio.Types.PhoneNumber(MobileNo)
                    );

                    R1.Status = 1;
                    R1.Message = "E-Mail Send Successfully.";

                    ExceptionlessClient.Default.SubmitLog("Text Send");
                }
                else
                {
                    R1.Status = 0;
                    R1.Message = "E-Mail Not Send.";
                }
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog("Text Failed");
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public void LogError(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            string path = Server.MapPath("~/ErrorLog/ErrorLog.txt");
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }

    }

}
