﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using MVCDatatable;
using System.Data;
using Newtonsoft.Json;
using System.Configuration;
using System.Data.SqlClient;

namespace MVCDatatable.Controllers
{
    public class ParkingLotController : Controller
    {
        string CN = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        Models.ReturnMSG R1 = new Models.ReturnMSG();
        Models.MyFunctions my = new Models.MyFunctions();
        
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        string sql = "";
        
        //
        // GET: /ParkingLot/

        public ActionResult Index()
        {
            ViewBag.Username = "GUEST";
            if (Session["Username"] != null)
            {
                string Username = Session["Username"].ToString();
                ViewBag.Username = Username;
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }


        [HttpPost]
        public ActionResult LoadData()
        {
            sql = "select * from ParkingLot";
            DataTable dt = new DataTable();
            dt = my.GetTable(sql);

            var jsonAllData = JsonConvert.SerializeObject(dt, Formatting.Indented);
            dynamic data = serializer.Deserialize(jsonAllData, typeof(object));

            return Json(new { data }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataByID(int ID)
        {
            sql = "select * from ParkingLot where Id=" + ID;
            DataTable dt = new DataTable();
            dt = my.GetTable(sql);
            var jsonAllData = JsonConvert.SerializeObject(dt, Formatting.Indented);
            dynamic data = serializer.Deserialize(jsonAllData, typeof(object));
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Add(ParkingLot parkinglot)
        {
            try
            {


                //================================Generate MaxCode==================================
                int MaxCode = 0;
                string LotId = "";
                SqlDataAdapter daMax = new SqlDataAdapter("SELECT MAX([MaxCode]) FROM [ParkingLot]", this.CN);
                DataSet dsMax = new DataSet();
                daMax.Fill(dsMax);
                if (dsMax.Tables[0].Rows[0][0] != DBNull.Value)
                {
                    MaxCode = Convert.ToInt32(dsMax.Tables[0].Rows[0][0]);
                }
                MaxCode++;
                if (MaxCode.ToString().Length == 1)
                {
                    LotId = string.Format("PARK-000000{0}", MaxCode.ToString());
                }
                else if (MaxCode.ToString().Length == 2)
                {
                    LotId = string.Format("PARK-00000{0}", MaxCode.ToString());
                }
                else if (MaxCode.ToString().Length == 3)
                {
                    LotId = string.Format("PARK-0000{0}", MaxCode.ToString());
                }
                else if (MaxCode.ToString().Length == 4)
                {

                    LotId = string.Format("PARK-000{0}", MaxCode.ToString());
                }
                else if (MaxCode.ToString().Length == 5)
                {
                    LotId = string.Format("PARK-00{0}", MaxCode.ToString());
                }
                else if (MaxCode.ToString().Length == 6)
                {
                    LotId = string.Format("PARK-0{0}", MaxCode.ToString());
                }
                else
                {
                    LotId = string.Format("PARK-{0}", MaxCode.ToString());
                }
                //==================================================================================

                sql = "insert into ParkingLot(MaxCode,LotId,LotName,Address,City,State,Country,Capacity,StartTime,EndTime,EventPrice,HourPrice,Lat,Long)values(" + MaxCode + ",'" + LotId + "','" + parkinglot.LotName.Trim() + "','" + parkinglot.Address.Trim() + "','" + parkinglot.City.Trim() + "','" + parkinglot.State.Trim() + "','" + parkinglot.Country.Trim() + "'," + parkinglot.Capacity + ",'" + parkinglot.StartTime + "','" + parkinglot.EndTime + "'," + parkinglot.EventPrice + "," + parkinglot.HourPrice + ",'" + parkinglot.Lat + "','" + parkinglot.Long + "')";
                int i = my.ExecuteWithReturnId(sql);
                if (i > 0)
                {
                    R1.Status = 1;
                    R1.Message = "Record Submitted Successfully.";
                }
                else
                {
                    R1.Status = 0;
                    R1.Message = "Error Occurred.";
                }
            }
            catch (Exception ex)
            {
                R1.Status = 0;
                R1.Message = ex.Message;
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Update(ParkingLot parkinglot)
        {
            try
            {
                
                sql = "update ParkingLot set LotName='" + parkinglot.LotName + "',Address='" + parkinglot.Address.Trim() + "',City='" + parkinglot.City.Trim() + "',State='" + parkinglot.State.Trim() + "',Country='" + parkinglot.Country.Trim() + "',Capacity=" + parkinglot.Capacity + ",StartTime='" + parkinglot.StartTime + "',EndTime='" + parkinglot.EndTime + "',EventPrice=" + parkinglot.EventPrice + ",HourPrice=" + parkinglot.HourPrice + ",Lat='" + parkinglot.Lat + "',Long='" + parkinglot.Long + "' where Id=" + parkinglot.Id;
                int i = my.Execute(sql);
                if (i > 0)
                {
                    R1.Status = 1;
                    R1.Message = "Record Updated Successfully.";
                }
                else
                {
                    R1.Status = 0;
                    R1.Message = "Error Occurred.";
                }
            }
            catch (Exception ex)
            {
                R1.Status = 0;
                R1.Message = ex.Message;
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }


        public JsonResult Delete(int ID)
        {
            try
            {
                sql = "delete from ParkingLot  where Id=" + ID;
                int i = my.Execute(sql);
                if (i > 0)
                {
                    R1.Status = 1;
                    R1.Message = "Record Deleted Successfully.";
                }
                else
                {
                    R1.Status = 0;
                    R1.Message = "Error in Query.";
                }
            }
            catch (Exception ex)
            {
                R1.Status = 0;
                R1.Message = ex.Message;
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }
         
    }
}
