﻿using Exceptionless;
using MVCDatatable.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.Configuration;
using System.Web.Mvc;

namespace MVCDatatable.Controllers
{
    public class AdvanceBookingController : Controller
    {
        Models.ReturnMSG R1 = new Models.ReturnMSG();
        Models.MyFunctions my = new Models.MyFunctions();

        string CN = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();


        public ActionResult Index()
        {
            ExceptionlessClient.Default.SubmitLog("Application starting up");
            GetEventData();
            GetParkingData();
            GetParkingLotWithEvent();
            GetParkingLotWithBundle();
            return View();
        }

        public ActionResult Success()
        {
            ExceptionlessClient.Default.SubmitLog("Advanced Booking Success");
            return View();
        }
        public ActionResult Fail()
        {
            ExceptionlessClient.Default.SubmitLog("Advanced Booking Failed");
            return View();
        }

        public JsonResult GetParkingData()
        {
            try
            {
                string sql = "SELECT ParkingLot.*, (select count(LotId) from BookingMaster where ParkingLot.Id = BookingMaster.LotId) as Booked from ParkingLot where Status = 'True' ";
                DataTable xDt = new DataTable();
                xDt = my.GetTable(sql);

                List<GetParkingData> parking = new List<GetParkingData>();
                if (xDt.Rows.Count > 0)
                {
                    for (int i = 0; i < xDt.Rows.Count; i++)
                    {
                        GetParkingData dmt2 = new GetParkingData();
                        dmt2.Id = xDt.Rows[i]["Id"].ToString();
                        dmt2.LotId = xDt.Rows[i]["LotId"].ToString();
                        dmt2.LotName = xDt.Rows[i]["LotName"].ToString();
                        dmt2.Address = xDt.Rows[i]["Address"].ToString();
                        dmt2.City = xDt.Rows[i]["City"].ToString();
                        dmt2.State = xDt.Rows[i]["State"].ToString();
                        dmt2.Country = xDt.Rows[i]["Country"].ToString();
                        dmt2.ZipCode = xDt.Rows[i]["ZipCode"].ToString();
                        dmt2.DistanceFromVanue = xDt.Rows[i]["DistanceFromVanue"].ToString();
                        dmt2.Capacity = Convert.ToInt32(xDt.Rows[i]["Capacity"].ToString());
                        dmt2.Booked = Convert.ToInt32(xDt.Rows[i]["Booked"].ToString());
                        dmt2.Available = dmt2.Capacity - dmt2.Booked;
                        dmt2.StartTime = xDt.Rows[i]["StartTime"].ToString();
                        dmt2.EndTime = xDt.Rows[i]["EndTime"].ToString();
                        dmt2.EventPrice = Math.Round(Convert.ToDecimal(xDt.Rows[i]["EventPrice"].ToString()), 2);
                        dmt2.HourPrice = Math.Round(Convert.ToDecimal(xDt.Rows[i]["HourPrice"].ToString()), 2);
                        dmt2.Lat = xDt.Rows[i]["Lat"].ToString();
                        dmt2.Lat = xDt.Rows[i]["Long"].ToString();
                        dmt2.MapLink = xDt.Rows[i]["MapLink"].ToString();

                        parking.Add(dmt2);
                    }
                }

                DataTable xTotalCount = new DataTable();
                xTotalCount = my.GetTable(sql);     

                ViewBag.ParkingDataList = "";

                R1.Status = 1;
                R1.Message = "Successfully.";

                ExceptionlessClient.Default.SubmitLog("Get Parking Data");
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEventData()
        {
            try
            {
                string sql = "Select Id, EventName, City, Prize from [dbo].[Events] where [IsActive] = 'True'";
                DataTable xDt = new DataTable();
                xDt = my.GetTable(sql);

                List<Events> events = new List<Events>();
                if (xDt.Rows.Count > 0)
                {
                    for (int i = 0; i < xDt.Rows.Count; i++)
                    {
                        Events eventss = new Events();
                        eventss.Id = Convert.ToInt32(xDt.Rows[i]["Id"].ToString());
                        eventss.EventName = xDt.Rows[i]["EventName"].ToString();
                        eventss.City = xDt.Rows[i]["City"].ToString();
                        eventss.Prize = xDt.Rows[i]["Prize"].ToString();
                        events.Add(eventss);
                    }
                }

                DataTable xTotalCount = new DataTable();
                xTotalCount = my.GetTable(sql);

                ViewBag.eventss = events;

                R1.Status = 1;
                R1.Message = "Successfully.";
                ExceptionlessClient.Default.SubmitLog("Get Events Data");
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetEventName()
        {
            try
            {
                string sql = "Select Id,EventName from [dbo].[Events] where IsActive = 'True' order By [EventDate] desc";
                DataTable xDt = new DataTable();
                xDt = my.GetTable(sql);

                List<EventList> eventlist = new List<EventList>();
                if (xDt.Rows.Count > 0)
                {
                    for (int i = 0; i < xDt.Rows.Count; i++)
                    {
                        EventList eventList1 = new EventList();
                        eventList1.EventId = Convert.ToInt32(xDt.Rows[i]["Id"]);
                        eventList1.EventName = xDt.Rows[i]["EventName"].ToString();

                        eventlist.Add(eventList1);
                    }
                }
                return Json(eventlist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLocation(int Id)
        {
            string sql = "SELECT * from ParkingLot where Status='True' and Id= " + Id;
            DataTable xDt = new DataTable();
            xDt = my.GetTable(sql);
            var jsonAllData = JsonConvert.SerializeObject(xDt, Formatting.Indented);
            dynamic data = serializer.Deserialize(jsonAllData, typeof(object));
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBookingDetails(int ID)
        {
            try
            {
                ExceptionlessClient.Default.SubmitLog("Get Booking Details");

                string sql = " select BookingMaster.*,ParkingLot.LotName, concat(ParkingLot.Address ,', ',ParkingLot.City ,', ',ParkingLot.State ,', ',ParkingLot.Country ,'',ParkingLot.ZipCode) as [Address], ParkingLot.Capacity,ParkingLot.StartTime,ParkingLot.EndTime,ParkingLot.MapLink from BookingMaster";
                sql += " Left Join ParkingLot on ParkingLot.Id = BookingMaster.LotId ";
                sql += " where BookingMaster.Id=" + ID;

                DataTable dt = new DataTable();
                dt = my.GetTable(sql);
                var jsonAllData = JsonConvert.SerializeObject(dt, Formatting.Indented);
                dynamic data = serializer.Deserialize(jsonAllData, typeof(object));
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetParkingLotWithEvent()
        {
            try
            {
                string sql = "select [dbo].[ParkinglotWithEvent].*, (select COUNT(LotId) From BookingMaster where ParkinglotWithEvent.Id = BookingMaster.LotId) as Booked FROM [dbo].[ParkinglotWithEvent] where [Status] = 'True'";
                DataTable xDt = new DataTable();
                xDt = my.GetTable(sql);

                List<EventParkingLotView> eventparkinglotview = new List<EventParkingLotView>();
                if (xDt.Rows.Count > 0)
                {
                    for (int i = 0; i < xDt.Rows.Count; i++)
                    {
                        EventParkingLotView eventparkinglot = new EventParkingLotView();
                        eventparkinglot.Id = Convert.ToInt32(xDt.Rows[i]["Id"].ToString());
                        eventparkinglot.EventName = xDt.Rows[i]["EventName"].ToString();
                        eventparkinglot.LotId = xDt.Rows[i]["LotId"].ToString();
                        eventparkinglot.LotName = xDt.Rows[i]["LotName"].ToString();
                        eventparkinglot.Address = xDt.Rows[i]["Address"].ToString();
                        eventparkinglot.City = xDt.Rows[i]["City"].ToString();
                        eventparkinglot.State = xDt.Rows[i]["State"].ToString();
                        eventparkinglot.Country = xDt.Rows[i]["Country"].ToString();
                        eventparkinglot.ZipCode = xDt.Rows[i]["ZipCode"].ToString();
                        eventparkinglot.Capacity = Convert.ToInt32(xDt.Rows[i]["Capacity"].ToString());
                        eventparkinglot.DistanceFromVanue = xDt.Rows[i]["DistanceFromVanue"].ToString();
                        eventparkinglot.StartTime = xDt.Rows[i]["StartTime"].ToString();
                        eventparkinglot.EndTime = xDt.Rows[i]["EndTime"].ToString();
                        eventparkinglot.EventPrice = xDt.Rows[i]["EventPrice"].ToString();
                        eventparkinglot.HourPrice = xDt.Rows[i]["HourPrice"].ToString();
                        eventparkinglot.Lat = xDt.Rows[i]["Lat"].ToString();
                        eventparkinglot.Long = xDt.Rows[i]["Long"].ToString();
                        eventparkinglot.Booked = Convert.ToInt32(xDt.Rows[i]["Booked"].ToString());
                        eventparkinglot.Available = eventparkinglot.Capacity - eventparkinglot.Booked;
                        eventparkinglot.MapLink = xDt.Rows[i]["MapLink"].ToString();
                        eventparkinglot.Status = xDt.Rows[i]["Status"].ToString();
                        eventparkinglot.CreatedDate = xDt.Rows[i]["CreatedDate"].ToString();
                        eventparkinglot.CreatedBy = xDt.Rows[i]["CreatedBy"].ToString();
                        eventparkinglot.ModifiedDate = xDt.Rows[i]["ModifiedDate"].ToString();
                        eventparkinglot.ModifiedBy = xDt.Rows[i]["ModifiedBy"].ToString();

                        eventparkinglotview.Add(eventparkinglot);
                    }
                }
                ViewBag.EventParkingLot = eventparkinglotview;
                return Json(eventparkinglotview, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetSelectedEventInfo(int Id)
        {
            var model = new EventInfo();
            try
            {
                string sql = "select [EventName],[EventDate] from [dbo].[Events] where Id = " + Id;
                DataTable xDt = new DataTable();
                xDt = my.GetTable(sql);
                
                if(xDt.Rows.Count > 0)
                {
                    model.EventName = xDt.Rows[0]["EventName"].ToString();
                    if(xDt.Rows[0]["EventDate"] == null || Convert.ToString(xDt.Rows[0]["EventDate"]) == "")
                    {
                        model.dateofEvent = null;
                    }
                    else
                    {
                        DateTime date = Convert.ToDateTime(xDt.Rows[0]["EventDate"]);
                        var dates = date.ToShortDateString();
                        model.dateofEvent = dates;
                    }
                }
                ViewBag.eventdetail = model;
                return PartialView("GetSelectedEventInfo", model);
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return PartialView("GetSelectedEventInfo", model);
        }

        [HttpGet]
        public ActionResult GetParkingLotWithBundle()
        {
            try
            {
                int EventId = int.Parse(WebConfigurationManager.AppSettings["EventId"]);
                string sql = "select [dbo].[ParkingLotWithBundle].*,(select COUNT(LotId) From BookingMaster bm where ParkingLotWithBundle.Id = bm.LotId and bm.[EventId] = "+ EventId +") as Booked FROM [dbo].[ParkingLotWithBundle] where [Status] = 'True' and [Id] = 3";
                DataTable xDt = new DataTable();
                xDt = my.GetTable(sql);

                List<EventParkingLotView> eventparkinglotview = new List<EventParkingLotView>();
                if (xDt.Rows.Count > 0)
                {
                    for (int i = 0; i < xDt.Rows.Count; i++)
                    {
                        EventParkingLotView eventparkinglot = new EventParkingLotView();
                        eventparkinglot.Id = Convert.ToInt32(xDt.Rows[i]["Id"].ToString());
                        eventparkinglot.LotId = xDt.Rows[i]["LotId"].ToString();
                        eventparkinglot.LotName = xDt.Rows[i]["LotName"].ToString();
                        eventparkinglot.Address = xDt.Rows[i]["Address"].ToString();
                        eventparkinglot.City = xDt.Rows[i]["City"].ToString();
                        eventparkinglot.State = xDt.Rows[i]["State"].ToString();
                        eventparkinglot.Country = xDt.Rows[i]["Country"].ToString();
                        eventparkinglot.ZipCode = xDt.Rows[i]["ZipCode"].ToString();
                        eventparkinglot.Capacity = Convert.ToInt32(xDt.Rows[i]["Capacity"].ToString());
                        eventparkinglot.DistanceFromVanue = xDt.Rows[i]["DistanceFromVanue"].ToString();
                        eventparkinglot.StartTime = xDt.Rows[i]["StartTime"].ToString();
                        eventparkinglot.EndTime = xDt.Rows[i]["EndTime"].ToString();
                        eventparkinglot.EventPrice = xDt.Rows[i]["EventPrice"].ToString();
                        eventparkinglot.HourPrice = xDt.Rows[i]["HourPrice"].ToString();
                        eventparkinglot.Lat = xDt.Rows[i]["Lat"].ToString();
                        eventparkinglot.Long = xDt.Rows[i]["Long"].ToString();
                        eventparkinglot.Booked = Convert.ToInt32(xDt.Rows[i]["Booked"].ToString());
                        eventparkinglot.Available = eventparkinglot.Capacity - eventparkinglot.Booked;
                        eventparkinglot.MapLink = xDt.Rows[i]["MapLink"].ToString();
                        eventparkinglot.Status = xDt.Rows[i]["Status"].ToString();
                        eventparkinglot.CreatedDate = xDt.Rows[i]["CreatedDate"].ToString();
                        eventparkinglot.CreatedBy = xDt.Rows[i]["CreatedBy"].ToString();
                        eventparkinglot.ModifiedDate = xDt.Rows[i]["ModifiedDate"].ToString();
                        eventparkinglot.ModifiedBy = xDt.Rows[i]["ModifiedBy"].ToString();

                        eventparkinglotview.Add(eventparkinglot);
                    }
                }
                ViewBag.BundleEventParkingLot = eventparkinglotview;
                return Json(eventparkinglotview, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetSelectedEventParkingLots(int id)
        {
            var model = new List<EventParkingLotView>();
            try
            {
                string sql = "select [dbo].[ParkinglotWithEvent].*, (select COUNT(LotId) From BookingMaster bm where ParkinglotWithEvent.Id = bm.LotId and bm.[EventId] = "+ id +") as Booked FROM [dbo].[ParkinglotWithEvent] where [Status] = 'True' and Id = 3 and eventId =" + id;
                DataTable xDt = new DataTable();
                xDt = my.GetTable(sql);

                if (xDt.Rows.Count > 0)
                {
                    for (int i = 0; i < xDt.Rows.Count; i++)
                    {
                        EventParkingLotView eventparkinglot = new EventParkingLotView();
                        eventparkinglot.Id = Convert.ToInt32(xDt.Rows[i]["Id"].ToString());
                        //eventparkinglot.EventName = xDt.Rows[i]["EventName"].ToString();
                        eventparkinglot.LotId = xDt.Rows[i]["LotId"].ToString();
                        eventparkinglot.LotName = xDt.Rows[i]["LotName"].ToString();
                        eventparkinglot.Address = xDt.Rows[i]["Address"].ToString();
                        eventparkinglot.City = xDt.Rows[i]["City"].ToString();
                        eventparkinglot.State = xDt.Rows[i]["State"].ToString();
                        eventparkinglot.Country = xDt.Rows[i]["Country"].ToString();
                        eventparkinglot.ZipCode = xDt.Rows[i]["ZipCode"].ToString();
                        eventparkinglot.Capacity = Convert.ToInt32(xDt.Rows[i]["Capacity"].ToString());
                        eventparkinglot.DistanceFromVanue = xDt.Rows[i]["DistanceFromVanue"].ToString();
                        eventparkinglot.StartTime = xDt.Rows[i]["StartTime"].ToString();
                        eventparkinglot.EndTime = xDt.Rows[i]["EndTime"].ToString();
                        eventparkinglot.EventPrice = xDt.Rows[i]["EventPrice"].ToString();
                        eventparkinglot.HourPrice = xDt.Rows[i]["HourPrice"].ToString();
                        eventparkinglot.Lat = xDt.Rows[i]["Lat"].ToString();
                        eventparkinglot.Long = xDt.Rows[i]["Long"].ToString();
                        eventparkinglot.Booked = Convert.ToInt32(xDt.Rows[i]["Booked"].ToString());
                        eventparkinglot.Available = eventparkinglot.Capacity - eventparkinglot.Booked;
                        eventparkinglot.MapLink = xDt.Rows[i]["MapLink"].ToString();
                        eventparkinglot.Status = xDt.Rows[i]["Status"].ToString();
                        eventparkinglot.CreatedDate = xDt.Rows[i]["CreatedDate"].ToString();
                        eventparkinglot.CreatedBy = xDt.Rows[i]["CreatedBy"].ToString();
                        eventparkinglot.ModifiedDate = xDt.Rows[i]["ModifiedDate"].ToString();
                        eventparkinglot.ModifiedBy = xDt.Rows[i]["ModifiedBy"].ToString();

                        model.Add(eventparkinglot);
                    }
                }
                ViewBag.eventparkinglotvalue = model;
                return PartialView("GetSelectedEventParkingLots", model);
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return PartialView("GetSelectedEventParkingLots", model);
        }

        public ActionResult MakeBookings(BookingMaster bm)
        {
            ExceptionlessClient.Default.SubmitLog("Booking Started");

            try
            {
                string StripKey = System.Web.Configuration.WebConfigurationManager.AppSettings["StripKey"];

                string BookingCode = "";
                string xMobile = "";    
                int? EventId = null;

                string MobileNo = "+1" + bm.MobileNo;
                xMobile = MobileNo;
                bm.EventId = EventId;

                string Email = bm.Email;
                string _Transaction_Number = "-";

                Stripe _stripe = new Stripe();
                string stripe_result = _stripe.ProcessTransaction(StripKey, bm.Extra1, bm.Extra2, bm.Extra3, bm.Extra4, bm.Extra5, Convert.ToDecimal(bm.Price), Email, xMobile);

                if (!String.IsNullOrEmpty(stripe_result))
                {
                    _Transaction_Number = stripe_result;

                    int BookingId = 0;

                    int CustId = my.ExecuteWithReturnId("insert into Customer(CustomerName,Email,Mobile)values('" + bm.CustomerName + "','" + bm.Email + "','" + xMobile + "')");
                    if (CustId > 0)
                    {
                        BookingId = my.ExecuteWithReturnId("insert into BookingMaster(BookingDate,LotId,CustomerName,CustomerId,EventId,LicensePlate,Price,TransactionNumber,Email,MobileNo)values('" + DateTime.Now + "'," + bm.LotId + ",'" + bm.CustomerName + "'," + CustId + ",'" + bm.EventId + "','" + bm.LicensePlate + "'," + bm.Price + ",'" + _Transaction_Number + "','" + bm.Email + "','" + xMobile + "')");
                        if (BookingId > 0)
                        {
                            //================================Generate Booking Code==================================
                            int MaxCode = BookingId;
                            if (MaxCode.ToString().Length == 1)
                            {
                                BookingCode = string.Format("BKN-000000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 2)
                            {
                                BookingCode = string.Format("BKN-00000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 3)
                            {
                                BookingCode = string.Format("BKN-0000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 4)
                            {

                                BookingCode = string.Format("BKN-000{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 5)
                            {
                                BookingCode = string.Format("BKN-00{0}", MaxCode.ToString());
                            }
                            else if (MaxCode.ToString().Length == 6)
                            {
                                BookingCode = string.Format("BKN-0{0}", MaxCode.ToString());
                            }
                            else
                            {
                                BookingCode = string.Format("BKN-{0}", MaxCode.ToString());
                            }
                            my.Execute("Update BookingMaster set BookingCode='" + BookingCode + "' where Id = " + BookingId);
                            //==================================================================================

                            ExceptionlessClient.Default.SubmitLog("Payment Success");

                            //EmailSend(BookingId);
                            //SendText(BookingId);

                            ExceptionlessClient.Default.SubmitLog("Booking Successfull");
                        }
                    }

                    R1.Status = 1;
                    R1.Message = BookingId.ToString();
                }
                else
                {
                    _Transaction_Number = "Failed";
                    R1.Status = 0;
                    R1.Message = stripe_result;
                }
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog("Booking Failed");
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }

            return Json(R1, JsonRequestBehavior.AllowGet);
        }

    }
}
