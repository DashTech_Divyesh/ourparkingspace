﻿using Exceptionless;
using MVCDatatable.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;

namespace MVCDatatable.Controllers
{
    public class AdminController : Controller
    {
        Models.ReturnMSG R1 = new Models.ReturnMSG();
        Models.MyFunctions my = new Models.MyFunctions();

        string CN = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        string lotValue = ConfigurationManager.AppSettings["showparkingLot"];
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        public ActionResult Index()
        {
            if(Session["UserName"] != null && Session["Password"] != null && (string)Session["UserName"] == "AdminShany")
            {
                GetParkingData();
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Admin");
            }
        }

        public ActionResult Login(AdminUser user)
        {
            ViewBag.Message = null;
            Session["UserName"] = null;
            Session["Password"] = null;

            if (ModelState.IsValid)
            {
                if (user.IsValid(user.UserName, user.Password))
                {
                    Session["UserName"] = user.UserName;
                    Session["Password"] = user.Password;
                    if(user.RememberMe)
                    {
                        Response.Cookies["UserName"].Value = user.UserName;
                        Response.Cookies["Password"].Value = user.Password;
                        Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(10);
                        Response.Cookies["Password"].Expires = DateTime.Now.AddDays(10);
                    }
                    else
                    {
                        Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);
                    }
                    return RedirectToAction("Index", "Admin");
                }
                else
                {
                    ViewBag.Message = "Either UserName or Password Incorrect";
                    //ModelState.AddModelError("", "Login data is incorrect!");
                }
            }
            return View(user);
        }

        public ActionResult UpdatePrice()
        {
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session["UserName"] = null;
            Session["Password"] = null;
            return RedirectToAction("Login", "Admin");
        }

        public ActionResult UpdatePrices(int Id)
        {
            var model = new EventPrice();
            try
            {
                string sql = "select [Id],[LotName],[EventPrice] from [dbo].[ParkingLot] where [Id] = " + Id;
                DataTable xDt = new DataTable();
                xDt = my.GetTable(sql);


                if (xDt.Rows.Count > 0)
                {
                    model.Id = Convert.ToInt32(xDt.Rows[0]["Id"]);
                    model.eventPrice = Convert.ToDouble(xDt.Rows[0]["EventPrice"]);
                    model.LotName = xDt.Rows[0]["LotName"].ToString();

                }

                DataTable xTotalCount = new DataTable();
                xTotalCount = my.GetTable(sql);

                ViewBag.EventPrice = model;

                R1.Status = 1;
                R1.Message = "Successfully.";
                return View();
                //return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveUpdatedPrice(int id , string eventprice)
        {
            string sql = "Update [dbo].[ParkingLot] set [EventPrice] ="+ eventprice + "Where [Id] =" + id;
            DataTable xDt = new DataTable();
            xDt = my.GetTable(sql);
            return RedirectToAction("Index", "Admin");
        }

        public JsonResult GetParkingData()
        {
            try
            {
                int EventId = int.Parse(WebConfigurationManager.AppSettings["EventId"]);
                string sql = "SELECT ParkingLot.*, (select count(LotId) from BookingMaster bm where ParkingLot.Id = bm.LotId and bm.[EventId]= " + EventId + ") as Booked from ParkingLot where Status = 'True' order by [DistanceFromVanue] asc ";
                DataTable xDt = new DataTable();
                xDt = my.GetTable(sql);

                List<GetParkingData> parking = new List<GetParkingData>();
                if (xDt.Rows.Count > 0)
                {
                    for (int i = 0; i < xDt.Rows.Count; i++)
                    {
                        GetParkingData dmt2 = new GetParkingData();

                        dmt2.Id = xDt.Rows[i]["Id"].ToString();
                        dmt2.LotId = xDt.Rows[i]["LotId"].ToString();
                        dmt2.LotName = xDt.Rows[i]["LotName"].ToString();
                        dmt2.Address = xDt.Rows[i]["Address"].ToString();
                        dmt2.City = xDt.Rows[i]["City"].ToString();
                        dmt2.State = xDt.Rows[i]["State"].ToString();
                        dmt2.Country = xDt.Rows[i]["Country"].ToString();
                        dmt2.ZipCode = xDt.Rows[i]["ZipCode"].ToString();
                        dmt2.DistanceFromVanue = xDt.Rows[i]["DistanceFromVanue"].ToString();
                        dmt2.Capacity = Convert.ToInt32(xDt.Rows[i]["Capacity"].ToString());
                        dmt2.Booked = Convert.ToInt32(xDt.Rows[i]["Booked"].ToString());
                        dmt2.Available = dmt2.Capacity - dmt2.Booked;
                        dmt2.StartTime = xDt.Rows[i]["StartTime"].ToString();
                        dmt2.EndTime = xDt.Rows[i]["EndTime"].ToString();
                        dmt2.EventPrice = Math.Round(Convert.ToDecimal(xDt.Rows[i]["EventPrice"].ToString()), 2);
                        dmt2.RealPrice = Math.Round(Convert.ToDecimal(xDt.Rows[i]["RealPrice"].ToString()), 2);
                        dmt2.HourPrice = Math.Round(Convert.ToDecimal(xDt.Rows[i]["HourPrice"].ToString()), 2);
                        dmt2.Lat = xDt.Rows[i]["Lat"].ToString();
                        dmt2.Lat = xDt.Rows[i]["Long"].ToString();
                        dmt2.MapLink = xDt.Rows[i]["MapLink"].ToString();

                        parking.Add(dmt2);
                    }
                }

                DataTable xTotalCount = new DataTable();
                xTotalCount = my.GetTable(sql);

                ViewBag.ParkingDataList = parking;

                R1.Status = 1;
                R1.Message = "Successfully.";

                ExceptionlessClient.Default.SubmitLog("Get Parking Data");
            }
            catch (Exception ex)
            {
                ExceptionlessClient.Default.SubmitLog(ex.Message);
                ex.ToExceptionless().Submit();
            }
            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ContactUs()
        {
            return View();
        }

        public ActionResult Success()
        {
            ExceptionlessClient.Default.SubmitLog("Payment Success");
            return View();
        }

        public ActionResult Fail()
        {
            ExceptionlessClient.Default.SubmitLog("Payment Failed");
            return View();
        }

    }
}
