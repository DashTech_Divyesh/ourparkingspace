﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using MVCDatatable.Models;


namespace MVCDatatable.Controllers
{
    public class ChangePasswordController : Controller
    {
        ReturnMSG R1 = new ReturnMSG();
        MyFunctions my = new MyFunctions();
        DataTable dt = new DataTable();

        //
        // GET: /ChangePassword/

        public ActionResult Index()
        {
            if (Session["Username"] != null)
            {
                string UserId = Session["UserId"].ToString();
                string Username = Session["Username"].ToString();
                string Code = Session["UserId"].ToString();
                string FullName = Session["FullName"].ToString();
                string Role = Session["Role"].ToString();

                ViewBag.Username = Username;
                ViewBag.Role = Role;

                return View();

            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        private string Encrypt(string clearText)
        {
            string EncryptionKey = "IMSV2OPIXA11162";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        private string Decrypt(string cipherText)
        {
            string EncryptionKey = "IMSV2OPIXA11162";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        [HttpPost]        
        public JsonResult ChangePassword(ChangePassword CH)
        {
            try
            {
                if (Session["Username"] != null)
                {
                    string PWD = this.Encrypt(CH.NPassword);
                    string OPassword = this.Encrypt(CH.OPassword);

                    if (CH.NPassword == CH.CPassword)
                    {
                        string sql = "select * from UserMaster where Username='" + Session["Username"].ToString() + "' and Password='" + OPassword + "'";
                        my = new Models.MyFunctions();
                        dt = my.GetTable(sql);
                        if (dt.Rows.Count > 0)
                        {
                            R1.Status = 1;
                            R1.Message = "Password Changed Successfully.";
                        }
                        else
                        {
                            R1.Status = 0;
                            R1.Message = "Wrong Password.";
                        }
                    }
                    else
                    {
                        R1.Status = 0;
                        R1.Message = "New Password and Confirm Password Not Match.";
                    }
                }
                else
                {
                    R1.Status = 0;
                    R1.Message = "You are not authorised user.";
                }
            }
            catch (Exception ex)
            {
                R1.Status = 0;
                R1.Message = ex.Message;
            }

            return Json(R1, JsonRequestBehavior.AllowGet);
        }
    }
}
