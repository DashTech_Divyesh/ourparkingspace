﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using MVCDatatable.Models;

namespace MVCDatatable.Controllers
{
    public class LoginController : Controller
    {
        ReturnMSG R1 = new ReturnMSG();
        MyFunctions my = new MyFunctions();
        DataTable dt = new DataTable();
        
        // GET: Login
        public ActionResult Index()
        {
            return View();            
        }

        private string Encrypt(string clearText)
        {
            string EncryptionKey = "IMSV2OPIXA11162";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        private string Decrypt(string cipherText)
        {
            string EncryptionKey = "IMSV2OPIXA11162";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }


        [HttpPost]
        public JsonResult SignIn(Login L1)
        {
            try
            {
                
                string PWD = this.Encrypt(L1.Password);
                if (L1.Username != null || L1.Password != null || L1.Username != "" || L1.Password != "")
                {
                    string sql = "select * from UserMaster where IsActive =1 and  Username='" + L1.Username + "' and Password='"+ PWD + "'";
                    dt = new DataTable();
                    my = new Models.MyFunctions();
                    dt = my.GetTable(sql);
                    if (dt.Rows.Count > 0)
                    {
                        Session["UserId"] = dt.Rows[0]["EmployeeId"].ToString();                        
                        Session["Username"] = dt.Rows[0]["Username"].ToString();
                        Session["FullName"] = dt.Rows[0]["FullName"].ToString();
                        Session["Role"] = dt.Rows[0]["Role"].ToString();

                        if(Convert.ToInt32(dt.Rows[0]["IsActive"].ToString()) ==1)
                        {
                            R1.Status = 1;
                            R1.Message = "Login Successfully.";
                        }
                        else
                        {
                            R1.Status = 1;
                            R1.Message = "Your account is deactivated, please contact to admin.";
                        }
                    }
                    else
                    {
                        R1.Status = 0;
                        R1.Message = "Wrong username or password.";
                    }
                }
                else
                {
                    R1.Status = 0;
                    R1.Message = "Please Enter Username or Password.";
                }
            }
            catch (Exception ex)
            {
                R1.Status = 0;
                R1.Message = ex.Message;
            }

            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SignOut()
        {
            if (Session["Username"] != null)
            {
                Session.Clear();
                Session.Abandon();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]        
        public JsonResult ChangePassword(ChangePassword CH)
        {
            try
            {
                if (Session["Username"] != null)
                {
                    string PWD = this.Encrypt(CH.NPassword);
                    string OPassword = this.Encrypt(CH.OPassword);

                    if (CH.NPassword == CH.CPassword)
                    {
                        string sql = "select * from UserMaster where Username='" + Session["Username"].ToString() + "' and Password='" + OPassword + "'";
                        my = new Models.MyFunctions();
                        dt = my.GetTable(sql);
                        if (dt.Rows.Count > 0)
                        {
                            int kk = my.Execute("Update UserMaster set Password='" + PWD + "'  where Username='" + Session["Username"].ToString() + "'");
                            R1.Status = 1;
                            R1.Message = "Password Changed Successfully.";
                        }
                        else
                        {
                            R1.Status = 0;
                            R1.Message = "Wrong Password.";
                        }
                    }
                    else
                    {
                        R1.Status = 0;
                        R1.Message = "New Password and Confirm Password Not Match.";
                    }
                }
                else
                {
                    R1.Status = 0;
                    R1.Message = "You are not authorised user.";
                }

            }
            catch (Exception ex)
            {
                R1.Status = 0;
                R1.Message = ex.Message;
            }

            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ForgotPassword(Login L1)
        {
            try
            {
                if (L1.Username != null || L1.Username != "")
                {
                    string sql = "select * from UserMaster where Username='" + L1.Username + "'";
                    my = new Models.MyFunctions();
                    dt = my.GetTable(sql);
                    if (dt.Rows.Count > 0)
                    {
                        Random r = new Random();
                        int newpswd = r.Next(10000000, 99999999);
                        string body = PopulateBody(newpswd.ToString());
                        Mail.sendmail(L1.Username, body, "Forget Password");

                        my.Execute("Update UserMaster set Password='" + this.Encrypt(newpswd.ToString()) + "' where Username='" + L1.Username + "'");
                        R1.Status = 1;
                        R1.Message = "Password Sent Successfully.";
                    }
                    else
                    {
                        R1.Status = 0;
                        R1.Message = "Your e-mail id not registered.";
                    }
                }
                else
                {
                    R1.Status = 0;
                    R1.Message = "Your e-mail id not registered.";
                }
            }
            catch (Exception ex)
            {
                R1.Status = 0;
                R1.Message = ex.Message;
            }

            return Json(R1, JsonRequestBehavior.AllowGet);
        }

        public string PopulateBody(string no)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Views/Emailbody.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{number}", no);

            return body;
        }
        
    }
}