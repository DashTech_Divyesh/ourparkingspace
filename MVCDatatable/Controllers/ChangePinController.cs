﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using MVCDatatable;
using System.Data;
using System.Configuration;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;


namespace MVCDatatable.Controllers
{
    public class ChangePinController : Controller
    {
        Models.ReturnMSG R1 = new Models.ReturnMSG();
        Models.MyFunctions my = new Models.MyFunctions();

        string CN = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        //
        // GET: /ChangePin/

        public ActionResult Index()
        {
            if (Session["Username"] != null)
            {
                string UserId = Session["UserId"].ToString();
                string Username = Session["Username"].ToString();
                string Code = Session["UserId"].ToString();
                string FullName = Session["FullName"].ToString();
                string Role = Session["Role"].ToString();

                ViewBag.Username = Username;
                ViewBag.Role = Role;

                return View();

            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        } 

    }
}
