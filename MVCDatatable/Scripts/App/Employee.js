﻿var baseUrl = $("base").first().attr("href");


$(document).ready(function () {
    var table; 

    loadData();

    $("#Status").bootstrapSwitch();
     
    var ParentMenu = document.getElementById('LiControl');
    ParentMenu.className = "nav-item active open";

    var SubMenu = document.getElementById('ulControl');
    SubMenu.style.display = 'block';

    var MenuArrow = document.getElementById('arrowControl');
    MenuArrow.className = 'arrow open';

});


function loadData() {
    table = $("#myTable").DataTable({
        "processing": false, // for show progress bar
        "serverSide": false, // for process server side
        "filter": true, // this is for disable filter (search box)
        "bDestroy": true,
        "lengthMenu": [[10, 25, 50, 100, 500, 1000,5000], [10, 25, 50, 100, 500, 1000,5000]],
        "pageLength": 100,
        "orderMulti": false, // for disable multiple column at once
        //"dom": '<"top"i>rt<"bottom"lp><"clear">', // for hide default global search box // little confusion? don't worry I explained in the tutorial website
        "ajax": {
            "url": baseUrl + "/Employee/LoadData",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
                //{ "data": "UserId", "name": "UserId", "autoWidth": true },
                { "data": "FullName", "name": "FullName", "autoWidth": true },
                { "data": "Gender", "name": "Gender", "autoWidth": true },                
                { "data": "DepartmentName", "name": "DepartmentName", "autoWidth": true },
                { "data": "DesignationName", "name": "DesignationName", "autoWidth": true },
                { "data": "JoiningDate", "name": "JoiningDate", "autoWidth": true },
                { "data": "MobileNo", "name": "MobileNo", "autoWidth": true },
                { "data": "ParentName", "name": "ParentName", "autoWidth": true },
                { "data": "Role", "name": "Role", "autoWidth": true },                
                { "data": "Status", "name": "Status", "autoWidth": true },
                { "data": "ProfileImg", "width": "auto" },
                { "defaultContent": "<button class='btn yellow-haze' id='btnEdit'><i class='fa fa-edit'></i></button>", "width": "30px" },
                { "defaultContent": "<button class='btn red-haze' id='btnDelete'><i class='fa fa-trash'></i></button>", "width": "30px" }
        ],
        columnDefs: [
            {
                targets: [4],
                render: function (data, type, row) {
                    var re = /-?\d+/;
                    var m = re.exec(data);
                    var d = new Date(parseInt(m[0]));
                    var curr_date = d.getDate();
                    //var curr_month = d.getMonth() + 1; //Months are zero based
                    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    curr_month = months[d.getMonth()]
                    var curr_year = d.getFullYear();
                    var formatedDate = curr_date + "/" + curr_month + "/" + curr_year;
                    return formatedDate;
                }
            }
            ,
                {
                    targets: [8],
                    render: function (data, type, row) {
                        if (data == true) {
                            data = "<b style='color:green'>Active</b>";
                        }
                        else {
                            data = "<b style='color:red'>Inactive</b>";
                        }
                        return data;
                    }
                }
                ,
                {
                    targets: [9],
                    render: function (data, type, row) {
                        if (data != null && data != "") {
                            data = '<center><a href="' + baseUrl + '/Upload/' + data + '" target="_blank"><i class="fa fa-eye"></i></a><center>';
                        }
                        else {
                            data = ''
                        }
                        return data;
                    }
                }

        ]
        
    });

    $('#myTable tbody').unbind('click').on('click', 'button', function () {
        var data = table.row($(this).parents('tr')).data();
        if (this.id == "btnEdit") {
            return getbyID2(data["Id"]);
        } 
        if (this.id == "btnDelete") {
            Delele(data["Id"]);
        }
    });
}

function getbyID2(Id) {
    window.location = baseUrl + '/Employee/AddNew?Id=' + Id;
}


function Delele(Id) {
    var ans = confirm("Are you sure you want to delete this Record?");
    if (ans) {
        $.ajax({
            url: baseUrl + "/Employee/Delete/" + Id,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                if (result.Status == "0") {
                    toastr.error(result.Message, "Error");
                }
                else {
                    toastr.success(result.Message, "Success");
                }

                table.ajax.reload();
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
    }
}
 

