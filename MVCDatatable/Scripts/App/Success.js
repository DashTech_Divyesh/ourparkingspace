﻿var baseUrl = $("base").first().attr("href");

//Load Data in Table when documents is ready
$(document).ready(function () {
    var foo = getParameterByName('Trn');
    if (foo != null) {
        GetBookingDetails(foo);
    } 

    if (typeof history.pushState === "function") {
        history.pushState("jibberish", null, null);
        window.onpopstate = function () {
            history.pushState('newjibberish', null, null);
        };
    }
    else {
        var ignoreHashChange = true;
        window.onhashchange = function () {
            if (!ignoreHashChange) {
                ignoreHashChange = true;
                window.location.hash = Math.random();
            }
            else {
                ignoreHashChange = false;
            }
        };
    }

});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function GetBookingDetails(Id) {
    //

    $.ajax({
        url: baseUrl + "/Home/GetBookingDetails/" + Id,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            document.getElementById('lblLotName').innerText = result[0].LotName;
            document.getElementById('lblBookingNo').innerText = result[0].BookingCode;
            document.getElementById('lblTransactionNumber').innerText = result[0].TransactionNumber;
            
            var a = document.getElementById('MyMap'); 
            a.href = result[0].MapLink;

            //EmailSend(Id);
            //SendText(Id);
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}
 

function EmailSend(Id) {
    $.ajax({
        url: baseUrl + "/Booking/EmailSend/" + Id,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}

function SendText(Id) {
    $.ajax({
        url: baseUrl + "/Booking/SendText/" + Id,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}
