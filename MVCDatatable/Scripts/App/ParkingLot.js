﻿var baseUrl = $("base").first().attr("href");

$(document).ready(function () {
    var table; 

    LoadData();
     
    var ParentMenu = document.getElementById('LiControl');
    ParentMenu.className = "nav-item active open";

    var MenuArrow = document.getElementById('arrowControl');
    MenuArrow.className = 'arrow open';

    var SubMenu = document.getElementById('ulControl');
    SubMenu.style.display = 'block';

});

function LoadData() {
    table = $("#myTable").DataTable({
        "processing": false,
        "serverSide": false,
        "filter": true, 
        "bDestroy": true,
        "lengthMenu": [[10, 25, 50, 100, 500, 1000, 5000, 10000], [10, 25, 50, 100, 500, 1000, 5000, 10000]],
        "pageLength": 100,
        "orderMulti": false,         
        "ajax": {
            "url": baseUrl + "/ParkingLot/LoadData",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
                { "data": "LotId", "name": "LotId", "Width": "30px" },
                { "data": "LotName", "name": "LotName", "autoWidth": true },
                { "data": "Address", "name": "Address", "autoWidth": true },
                { "data": "StartTime", "name": "StartTime", "autoWidth": true },
                { "data": "EndTime", "name": "EndTime", "autoWidth": true },
                { "data": "EventPrice", "name": "EventPrice", "autoWidth": true },
                { "data": "HourPrice", "name": "HourPrice", "autoWidth": true },
                { "defaultContent": "<button class='btn yellow-haze' id='btnEdit'><i class='fa fa-edit'></i></button>", "width": "30px" },
                { "defaultContent": "<button class='btn red-haze' id='btnDelete'><i class='fa fa-trash'></i></button>", "width": "30px" }
        ]
        ,
        columnDefs: [
                {
                    targets: [7],
                    render: function (data, type, row) {
                        if (data == true) {
                            data = "<b style='color:green'>Active</b>";
                        }
                        else {
                            data = "<b style='color:red'>Inactive</b>";
                        }
                        return data;
                    }
                }
        ]
        
    });

    $('#myTable tbody').unbind('click').on('click', 'button', function () {
        var data = table.row($(this).parents('tr')).data();
        if (this.id == "btnEdit") {
            return GetDataByID(data["Id"]);
        } 
        if (this.id == "btnDelete") {
            Delele(data["Id"]);
        }
    });
}

function GetDataByID(Id) {
    document.getElementById('lblmyModalLabel').innerText = 'Update Bank Info.';

    $("#Id").css('border-color', 'lightgrey');
    $("#LotName").css('border-color', 'lightgrey');
    $("#Address").css('border-color', 'lightgrey');
    $("#City").css('border-color', 'lightgrey');
    $("#State").css('border-color', 'lightgrey');
    $("#Country").css('border-color', 'lightgrey');
    $("#Capacity").css('border-color', 'lightgrey');
    $('#Status').css('border-color', 'lightgrey');

    $.ajax({
        url: baseUrl + "/ParkingLot/GetDataByID/" + Id,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            //======LoadData To Control=============
            $('#Id').val(result[0].Id);
            $("#LotName").val(result[0].LotName);
            $("#Address").val(result[0].Address);
            $("#City").val(result[0].City);
            $("#LotName").val(result[0].LotName);
            $("#Address").val(result[0].Address);
            $("#Capacity").val(result[0].Capacity);

            var MyStatus = result[0].Status;
            if (MyStatus == true) {
                $('.make-switch').bootstrapSwitch('state', true);
            }
            else {
                $('.make-switch').bootstrapSwitch('state', false);
            }
            //======LoadData To Control=============

            $('#btnUpdate').show();
            $('#btnAdd').hide();
            $('#myModal').modal('show');

        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}

function Add() {

    if ($('#LotName').val().trim() == "") {
        $('#LotName').css('border-color', 'Red');
        toastr.error("Please Enter Bank Name.", "Error");
        $('#LotName').focus();
        return false;
    }
    else {
        $('#LotName').css('border-color', 'lightgrey');
    }

    if ($('#Address').val().trim() == "") {
        $('#Address').css('border-color', 'Red');
        toastr.error("Please Enter Branch Name.", "Error");
        $('#Address').focus();
        return false;
    }
    else {
        $('#Address').css('border-color', 'lightgrey');
    }
     
    if ($('#LotName').val().trim() == "") {
        $('#LotName').css('border-color', 'Red');
        toastr.error("Please Enter Account Name.", "Error");
        $('#LotName').focus();
        return false;
    }
    else {
        $('#LotName').css('border-color', 'lightgrey');
    }

    if ($('#Address').val().trim() == "") {
        $('#Address').css('border-color', 'Red');
        toastr.error("Please Enter Account No.", "Error");
        $('#Address').focus();
        return false;
    }
    else {
        $('#Address').css('border-color', 'lightgrey');
    }

    if ($('#City').val().trim() == "") {
        $('#City').css('border-color', 'Red');
        toastr.error("Please Enter IFSC Code.", "Error");
        $('#City').focus();
        return false;
    }
    else {
        $('#City').css('border-color', 'lightgrey');
    }

    //if ($('#Email').val().length != 0) {
    //    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    //    if (reg.test($('#Email').val().trim()) == false) {
    //        toastr.error("Invalid E-Mail Address.", "Error");
    //        $('#Email').focus();
    //        $('#Email').css('border-color', 'Red');
    //        return false;
    //    } else {
    //        $('#Email').css('border-color', 'lightgrey');
    //    }
    //}

    if ($('#Capacity').val().trim() == "") {
        $('#Capacity').css('border-color', 'Red');
        toastr.error("Please Enter Mobile No.", "Error");
        $('#Capacity').focus();
        return false;
    }
    else {
        $('#Capacity').css('border-color', 'lightgrey');
    }


    if ($('#Capacity').val().trim().length != 10) {
        $('#Capacity').css('border-color', 'Red');
        toastr.error("Please Enter 10 Digit Mobile No.", "Error");
        $('#Capacity').focus();
        return false;
    }
    else {
        $('#Capacity').css('border-color', 'lightgrey');
    }

    var MyStatus;
    if ($('#Status').is(":checked")) {
        MyStatus = "True";
    }
    else {
        MyStatus = "False";
    }

    var fileData = new FormData();

    fileData.append("Id", $("#Id").val());    
    fileData.append("LotName", $("#LotName").val());
    fileData.append("Address", $("#Address").val());
    fileData.append("City", $("#City").val());
    fileData.append("LotName", $("#LotName").val());
    fileData.append("Address", $("#Address").val());
    fileData.append("Capacity", $("#Capacity").val());
    fileData.append("Status", MyStatus);

    $.ajax({
        type: "POST",
        url: baseUrl + "/ParkingLot/Add",
        data: fileData,
        contentType: false,
        processData: false,
        dataType: "json",
        enctype: 'multipart/form-data',
        success: function (result) {
              
            if (result.Status == "0") {
                toastr.error(result.Message, "Error");
            }
            else {
                toastr.success(result.Message, "Success");

                //=========Clear ControlBox Value========
                $('#Id').val("");
                $("#LotName").val("");
                $("#Address").val("");
                $("#City").val("");
                $("#LotName").val("");
                $("#Address").val("");
                $("#Capacity").val("");
                $('.make-switch').bootstrapSwitch('state', true);
                //=========End ControlBox Value========

                //=========Hide Model========
                $('#myModal').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                //=========End Hide Model========
            }
            table.ajax.reload();
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

function Update() {

    if ($('#LotName').val().trim() == "") {
        $('#LotName').css('border-color', 'Red');
        toastr.error("Please Enter Bank Name.", "Error");
        $('#LotName').focus();
        return false;
    }
    else {
        $('#LotName').css('border-color', 'lightgrey');
    }

    if ($('#Address').val().trim() == "") {
        $('#Address').css('border-color', 'Red');
        toastr.error("Please Enter Branch Name.", "Error");
        $('#Address').focus();
        return false;
    }
    else {
        $('#Address').css('border-color', 'lightgrey');
    }

    if ($('#LotName').val().trim() == "") {
        $('#LotName').css('border-color', 'Red');
        toastr.error("Please Enter Account Name.", "Error");
        $('#LotName').focus();
        return false;
    }
    else {
        $('#LotName').css('border-color', 'lightgrey');
    }

    if ($('#Address').val().trim() == "") {
        $('#Address').css('border-color', 'Red');
        toastr.error("Please Enter Account No.", "Error");
        $('#Address').focus();
        return false;
    }
    else {
        $('#Address').css('border-color', 'lightgrey');
    }

    if ($('#City').val().trim() == "") {
        $('#City').css('border-color', 'Red');
        toastr.error("Please Enter IFSC Code.", "Error");
        $('#City').focus();
        return false;
    }
    else {
        $('#City').css('border-color', 'lightgrey');
    }

    //if ($('#Email').val().length != 0) {
    //    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    //    if (reg.test($('#Email').val().trim()) == false) {
    //        toastr.error("Invalid E-Mail Address.", "Error");
    //        $('#Email').focus();
    //        $('#Email').css('border-color', 'Red');
    //        return false;
    //    } else {
    //        $('#Email').css('border-color', 'lightgrey');
    //    }
    //}

    if ($('#Capacity').val().trim() == "") {
        $('#Capacity').css('border-color', 'Red');
        toastr.error("Please Enter Mobile No.", "Error");
        $('#Capacity').focus();
        return false;
    }
    else {
        $('#Capacity').css('border-color', 'lightgrey');
    }


    if ($('#Capacity').val().trim().length != 10) {
        $('#Capacity').css('border-color', 'Red');
        toastr.error("Please Enter 10 Digit Mobile No.", "Error");
        $('#Capacity').focus();
        return false;
    }
    else {
        $('#Capacity').css('border-color', 'lightgrey');
    }

    var MyStatus;
    if ($('#Status').is(":checked")) {
        MyStatus = "True";
    }
    else {
        MyStatus = "False";
    }

    var fileData = new FormData();

    fileData.append("Id", $("#Id").val());
    fileData.append("LotName", $("#LotName").val());
    fileData.append("Address", $("#Address").val());
    fileData.append("City", $("#City").val());
    fileData.append("LotName", $("#LotName").val());
    fileData.append("Address", $("#Address").val());
    fileData.append("Capacity", $("#Capacity").val());
    fileData.append("Status", MyStatus);

    $.ajax({
        type: "POST",
        url: baseUrl + "/ParkingLot/Update",
        data: fileData,
        contentType: false,
        processData: false,
        dataType: "json",
        enctype: 'multipart/form-data',
        success: function (result) {
             
            if (result.Status == "0") {
                toastr.error(result.Message, "Error");
            }
            else {
                toastr.success(result.Message, "Success");

                //=========Clear ControlBox Value========
                $('#Id').val("");
                $("#LotName").val("");
                $("#Address").val("");
                $("#City").val("");
                $("#LotName").val("");
                $("#Address").val("");
                $("#Capacity").val("");
                $('.make-switch').bootstrapSwitch('state', true);
                //=========End ControlBox Value========

                //=========Hide Model========
                $('#myModal').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                //=========End Hide Model========
            }
            table.ajax.reload();
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

function Delele(Id) {
    var ans = confirm("Are you sure you want to delete this Record?");
    if (ans) {
        $.ajax({
            url: baseUrl + "/ParkingLot/Delete/" + Id,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                if (result.Status == "0") {
                    toastr.error(result.Message, "Error");
                }
                else {
                    toastr.success(result.Message, "Success");
                }

                table.ajax.reload();
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
    }
}
 
function clearTextBox() {

    document.getElementById('lblmyModalLabel').innerText = 'Add Bank Info.';

    $('#Id').val("");
    $("#LotName").val("");
    $("#Address").val("");
    $("#City").val("");
    $("#LotName").val("");
    $("#Address").val("");
    $("#Capacity").val("");    
    $('.make-switch').bootstrapSwitch('state', true);

    $('#btnUpdate').hide();
    $('#btnAdd').show();

    $("#Id").css('border-color', 'lightgrey');
    $("#LotName").css('border-color', 'lightgrey');
    $("#Address").css('border-color', 'lightgrey');
    $("#City").css('border-color', 'lightgrey');
    $("#LotName").css('border-color', 'lightgrey');
    $("#Address").css('border-color', 'lightgrey');
    $("#Capacity").css('border-color', 'lightgrey');
    $('#Status').css('border-color', 'lightgrey');
}
 