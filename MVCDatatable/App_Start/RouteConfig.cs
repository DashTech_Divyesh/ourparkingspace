﻿using System.Web.Mvc;
using System.Web.Routing;

namespace MVCDatatable
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "backpageadmin",
               url: "backpageadmin/{action}",
               defaults: new { controller = "Admin", action = "Login" }
            );

            routes.MapRoute(
               name: "backpagelotowner",
               url: "backpagelotowner/{action}",
               defaults: new { controller = "LotOwner", action = "Login" }
            );

            routes.MapRoute(
               name: "backpagetowing",
               url: "backpagetowing/{action}",
               defaults: new { controller = "Towing", action = "Login" }
            );

            //routes.MapRoute(
            //   name: "userslot",
            //   url: "backpagelotowner/{action}",
            //   defaults: new { controller = "LotOwner", action = "GetParkingLotWithUser", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}